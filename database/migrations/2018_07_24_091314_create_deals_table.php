<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('deals', function (Blueprint $table) {
           $table->increments('id');
           $table->string('dealName');
           $table->longText('dealDescriptionPath')->nullable();
           $table->integer('dealUnitPrice');
           $table->integer('dealUnitDiscount');
           $table->integer('dealOwner');
           $table->integer('dealTotalPrice');
           $table->integer('dealVisits');
           $table->enum('dealStatus', ['Active', 'Deactive']);
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('deals');
     }
}
