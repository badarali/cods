<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('medias', function (Blueprint $table) {
           $table->increments('id');
           $table->enum('mediaType', ['Video', 'Image','Document']);
           $table->string('mediaExtension');
           $table->string('mediaPath');
           $table->enum('mediasstatus', ['Active', 'Deactive']);
           $table->integer('productId')->nullable();
           $table->integer('dealId')->nullable();
           $table->integer('userProfileId')->nullable();
           $table->integer('precidence')->nullable();
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('medias');
     }
}
