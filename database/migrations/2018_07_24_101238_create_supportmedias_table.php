<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportmediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('supportmedias', function (Blueprint $table) {
           $table->increments('id');
           $table->enum('mediaType', ['Video', 'Image','Document']);
           $table->string('mediaExtension');
           $table->integer('mediaPath');
           $table->integer('supportId')->nullable();
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('supportmedias');
     }
}
