<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('products', function (Blueprint $table) {
          $table->increments('id');
          $table->string('productName');
          $table->longText('productDescriptionPath')->nullable();
          $table->integer('productUnitPrice');
          $table->integer('productUnitDiscount');
          $table->integer('productOwner');
          $table->integer('productTotalPrice');
          $table->integer('productVisits');
          $table->enum('productStatus', ['Active', 'Deactive']);
          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
