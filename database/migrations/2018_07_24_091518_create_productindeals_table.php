<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductindealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('productindeals', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('productId')->nullable();
           $table->integer('dealId')->nullable();
           $table->enum('productindealsstatus', ['Active', 'Deactive']);
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('productindeals');
     }
}
