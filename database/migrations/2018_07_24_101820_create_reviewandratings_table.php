<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewandratingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('reviewandratings', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('byUserId');
           $table->integer('toUserId')->nullable();
           $table->integer('toDealId')->nullable();
           $table->integer('toProductId')->nullable();
           $table->integer('stars');
           $table->longText('reviewPath')->nullable();
           $table->enum('status', ['Active', 'Deactive']);
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('reviewandratings');
     }
}
