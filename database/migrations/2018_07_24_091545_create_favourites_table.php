<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('favourites', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('byUserId');
           $table->integer('toUserId')->nullable();
           $table->boolean('toProfile')->nullable();
           $table->integer('toProductId')->nullable();
           $table->integer('toDealId')->nullable();
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('favourites');
     }
}
