<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagemediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('messagemedias', function (Blueprint $table) {
           $table->increments('id');
           $table->enum('mediaType', ['Video', 'Image','Document']);
           $table->string('mediaExtension');
           $table->integer('mediaPath');
           $table->integer('messageId')->nullable();
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('messagemedias');
     }
}
