<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdermetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('ordermetas', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('productId')->nullable();
           $table->integer('dealId')->nullable();
           $table->integer('orderId')->nullable();
           $table->integer('quantity');
           $table->integer('orderconnectId');
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('ordermetas');
     }
}
