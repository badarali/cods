<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrontTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('front', function (Blueprint $table) {
          $table->increments('id');
          $table->string('contact')->nullable();
          $table->string('email')->nullable();
          $table->string('address')->nullable();
          $table->string('googlePlus')->nullable();
          $table->string('facebook')->nullable();
          $table->string('instagram')->nullable();
          $table->string('linkedin')->nullable();
          $table->string('twitter')->nullable();
          $table->string('youtube')->nullable();
          $table->string('logoPath');
          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
