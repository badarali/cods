<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsbidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('assetsbid', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sellerId');
            $table->dateTime('startTime');
            $table->dateTime('endTime');
            $table->integer('dealId')->nullable();
            $table->integer('productId')->nullable();
            $table->integer('baseAmount');
            $table->enum('status', ['run','win', 'loss']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
