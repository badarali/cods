<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('contacts', function (Blueprint $table) {
           $table->increments('id');
           $table->enum('contactsOf', ['Home', 'Office','Other']);
           $table->string('contactsBody');
           $table->integer('contactsOfUser');
           $table->enum('contactsstatus', ['Active', 'Deactive']);
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('contacts');
     }
}
