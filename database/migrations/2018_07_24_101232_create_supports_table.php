<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('supports', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('byUserId');
           $table->integer('toUserId')->nullable();
           $table->string('subject');
           $table->string('bodyPath');
           $table->enum('supportStatus', ['Open', 'Close', 'Process']);
           $table->enum('supportType', ['Sale', 'Order', 'Technical', 'Purchases', 'Delivery','Other']);
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('supports');
     }
}
