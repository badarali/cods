<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('orders', function (Blueprint $table) {
           $table->increments('id');
           $table->enum('orderStatus', ['Active', 'Process','Complete','Closed']);
           $table->integer('orderCalculatedSum');
           $table->integer('orderDiscount')->nullable();
           $table->integer('orderTotalPrice');
           $table->integer('orderOnAddressId');
           $table->integer('orderOnContactId');
           $table->integer('orderByUserId');
           $table->integer('orderToSellerId');
           $table->boolean('orderInCart');
           $table->integer('orderconnectId');
           $table->rememberToken();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('orders');
     }
}
