<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetBid extends Model
{
    protected $table = 'assetsbid';
}
