<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class FavouriteController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Favourite::all();
        foreach ($data as $key => $value) {

        }
        return view('favourite.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Favourite::where('favouriteesOfUser','=',$authenticatedUser->id);
        return View('favourite.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  public function favourites($type){
    $authenticatedUser=Auth::User();
    $message = "Favourites";
    if($authenticatedUser->role == 'Super'){
      if ($type=='product') {
        $favourites= \App\Favourite::all();
        $data=[];
        foreach ($favourites as $key => $value) {
          if (!empty($value->toProductId)) {
            $data[] = \App\Product::where('id',$value->toProductId)->first();
          }
        }
        return view('product.index', compact('data','message'));
      }
      else if ($type=='deal') {
        $favourites= \App\Favourite::all();
        $data=[];
        foreach ($favourites as $key => $value) {
          if ($value->toDealId) {
            $data[] = \App\Deal::where('id',$value->toDealId)->first();
          }
        }
        return view('deal.index', compact('data','message'));
      }
    }
    else if ($authenticatedUser->role == 'Admin') {
      if ($type=='product') {
        $favourites= \App\Favourite::all();
        $data=[];
        foreach ($favourites as $key => $value) {
          if (!empty($value->toProductId)) {
            $product = \App\Product::where('id',$value->toProductId)->where('productOwner',Auth::id())->first();
            if ($product!=null) {
              $data[] = $product;
            }
          }
        }
        return view('product.index', compact('data','message'));
      }
      else if ($type=='deal') {
        $favourites= \App\Favourite::all();
        $data=[];
        foreach ($favourites as $key => $value) {
          if ($value->toDealId) {
            $deal = \App\Deal::where('id',$value->toDealId)->where('dealOwner',Auth::id())->first();
            if ($deal!=null) {
              $data[] = $deal;
            }
          }
        }
        return view('deal.index', compact('data','message'));
      }
    }
    else if ($authenticatedUser->role == 'Client') {
      if ($type=='product') {
        $favourites= \App\Favourite::where('byUserId', Auth::id())->get();
        $data=[];
        foreach ($favourites as $key => $value) {
          if (!empty($value->toProductId)) {
            $product = \App\Product::where('id',$value->toProductId)->first();
            if ($product!=null) {
              $data[] = $product;
            }
          }
        }
        return view('product.index', compact('data','message'));
      }
      else if ($type=='deal') {
        $favourites= \App\Favourite::where('byUserId', Auth::id())->get();
        $data=[];
        foreach ($favourites as $key => $value) {
          if ($value->toDealId) {
            $deal = \App\Deal::where('id',$value->toDealId)->first();
            if ($deal!=null) {
              $data[] = $deal;
            }
          }
        }
        return view('deal.index', compact('data','message'));
      }
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('favourite.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
        $fav = new \App\Favourite;
        $fav->byUserId = $request->get('byUserId');
        if($request->get('type')=='Deal'){
            $fav->toDealId = $request->get('toDealId');
        }
        else if(($request->get('type')=='Product')){
            $fav->toProductId = $request->get('toProductId');
        }
        $fav->save();
          $message= "One Data Successfully saved";

        if ($request->get('type')=='Deal') {
           $data = \App\Deal::where('id', $request->get('toDealId'))->first();
           $assetcategories = \App\AssetCategory::where('dealId', $request->get('toDealId'))->get();
           foreach ($assetcategories as $key => $value) {
             if ($key==0) {
               $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
             }
             else{
               $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
               $categories = $categories->merge($result);
             }
           }
           $similarDeals = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$request->get('toDealId'))->get();
           $count=0;
           foreach ($categories as $key => $value) {
             if ($value->dealId!=$request->get('toDealId')) {
               $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$request->get('toDealId'))->get();
               if ($count==0) {
                 $similarDeals = $result;
                 $count++;
               }
               else{
                 $similarDeals = $similarDeals->merge($result);
               }
             }
           }
           $deals = array();
           $dealids = array();
           if (!$similarDeals->isEmpty()) {
             foreach ($similarDeals as $key => $value) {
               if (!in_array($value->dealId, $dealids)) {
                 $dealids[] = $value->dealId;
                 $deals[] = \App\Deal::findOrFail($value->dealId);
               }
             }
           }
            return View('deal.show',compact('data','message', 'deals'));
        }
        elseif (($request->get('type')=='Product')) {
          $data = \App\Product::where('id', $request->get('toProductId'))->first();
          $assetcategories = \App\AssetCategory::where('productId', $request->get('toProductId'))->get();
          foreach ($assetcategories as $key => $value) {
            if ($key==0) {
              $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            }
            else{
              $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
              $categories = $categories->merge($result);
            }
          }
          $count=0;
          $similarProducts=\App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$request->get('toProductId'))->get();;
          foreach ($categories as $key => $value) {
            if ($value->productId!=$request->get('toProductId')) {
              $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$request->get('toProductId'))->get();
              if ($count==0) {
                $similarProducts = $result;
                $count++;
              }
              else{
                $similarProducts = $similarProducts->merge($result);
              }
            }
          }

          $products = array();
          $productids = array();
          if (!$similarProducts->isEmpty()) {
            foreach ($similarProducts as $key => $value) {
              if (!in_array($value->productId, $productids)) {
                $productids[] = $value->productId;
                $products[] = \App\Product::findOrFail($value->productId);
              }
            }
          }
            return View('product.show',compact('data','message','products'));
        }


    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Favourite::findOrFail($id);
      $message= "One data Requested";
      return view('favourite.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Favourite::findOrFail($id);
    $message= "One data Requested";
    return view('favourite.edit', compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = Favourite::findOrFail($id);
    $input = Request::all();
    $data->update($input);
    $message="One data updated";
    return view('favourite.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    if(Auth::User()){
      $data = \App\Favourite::findOrFail($id);
      $data->delete();
      $message="One data deleted";
      if($request->get('type')=='Deal'){
        $data = \App\Deal::where('id', $request->get('dealId'))->first();
        $assetcategories = \App\AssetCategory::where('dealId', $request->get('dealId'))->get();
        foreach ($assetcategories as $key => $value) {
          if ($key==0) {
            $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          }
          else{
            $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            $categories = $categories->merge($result);
          }
        }
        $similarDeals = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$request->get('dealId'))->get();
        $count=0;
        foreach ($categories as $key => $value) {
          if ($value->dealId!=$request->get('dealId')) {
            $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$request->get('dealId'))->get();
            if ($count==0) {
              $similarDeals = $result;
              $count++;
            }
            else{
              $similarDeals = $similarDeals->merge($result);
            }
          }
        }
        $deals = array();
        $dealids = array();
        if (!$similarDeals->isEmpty()) {
          foreach ($similarDeals as $key => $value) {
            if (!in_array($value->dealId, $dealids)) {
              $dealids[] = $value->dealId;
              $deals[] = \App\Deal::findOrFail($value->dealId);
            }
          }
        }
        return View('deal.show',compact('message','data','deals'));
      }
      else if ($request->get('type')=='Product') {
        $data = \App\Product::where('id', $request->get('productId'))->first();
        $assetcategories = \App\AssetCategory::where('productId', $request->get('productId'))->get();
        foreach ($assetcategories as $key => $value) {
          if ($key==0) {
            $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          }
          else{
            $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            $categories = $categories->merge($result);
          }
        }
        $count=0;
        $similarProducts=\App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$request->get('productId'))->get();;
        foreach ($categories as $key => $value) {
          if ($value->productId!=$request->get('productId')) {
            $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$request->get('productId'))->get();
            if ($count==0) {
              $similarProducts = $result;
              $count++;
            }
            else{
              $similarProducts = $similarProducts->merge($result);
            }
          }
        }

        $products = array();
        $productids = array();
        if (!$similarProducts->isEmpty()) {
          foreach ($similarProducts as $key => $value) {
            if (!in_array($value->productId, $productids)) {
              $productids[] = $value->productId;
              $products[] = \App\Product::findOrFail($value->productId);
            }
          }
        }
        return View('product.show',compact('message','data','products'));
      }
    }
    else{
      return View('auth.login');
    }
  }
}
