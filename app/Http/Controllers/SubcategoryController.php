<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
Use \App\Subcategory;

class SubcategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Subcategory::all();
        return view('subcategory.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Subcategory::where('subcategoryesOfUser','=',$authenticatedUser->id);
        return View('subcategory.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('subcategory.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      //$data= Request::all();
      //$data= Subcategory::create($data);
      $subcategory = new \App\Subcategory;
      $subcategory->childCategory = $request->get('childCategory');
      $subcategory->parentCategory = $request->get('parentCategory');
      $subcategory->categorystatus = $request->get('categorystatus');
      $subcategory->save();
      $data= Subcategory::all();
      $message= "One Data Successfully saved";
      return View('subcategory.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Subcategory::findOrFail($id);
      $message= "One data Requested";
      return view('subcategory.show',compact('date','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Subcategory::findOrFail($id);
    $message= "One data Requested";
    return view('subcategory.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = Subcategory::findOrFail($id);
    //$input = Request::all();
    $data->childCategory = $request->get('childCategory');
    $data->parentCategory = $request->get('parentCategory');
    $data->categorystatus = $request->get('categorystatus');
    $data->save();
    $data= Subcategory::all();
    //$data->update($input);
    $message="One data updated";
    return View('subcategory.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Subcategory::findOrFail($id);
    $data->delete();
    $message="One data deleted";
    $data= Subcategory::all();
    return View('subcategory.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }
}
