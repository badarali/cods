<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Deal;
use Illuminate\Support\Facades\Input;
use \App\Media;
use \App\Product;
use File;

class MediaController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Media::all();
        return view('media.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Media::where('mediaesOfUser','=',$authenticatedUser->id);
        return View('media.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('media.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $data= Request::all();
      $data= Media::create($data);
      $message= "One Data Successfully saved";
      return View('media.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Media::findOrFail($id);
      $message= "One data Requested";
      return view('media.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Media::findOrFail($id);
    $message= "One data Requested";
    return view('media.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = Media::findOrFail($id);
    $input = Request::all();
    $data->update($input);
    $message="One data updated";
    return view('media.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request,$id)
  {
    $type = $request->get('type');
    $otherid = $request->get('otherid');

    if(Auth::User()){
      $data = Media::findOrFail($id);

      if($type=="Deal"){
        if ($data->mediaType=='Image') {
          $filename='deal/images/'.$data->mediaPath;
        }
        else{
          $filename='deal/videos/'.$data->mediaPath;
        }
        File::delete($filename);
        $data->delete();
        $message="One data deleted";
        $data = Deal::findOrFail($otherid);
        $media = Media::where('dealId',$otherid)->get();
        $selectedCategories = \App\AssetCategory::where('dealId', $otherid)->get();
        return view('deal.edit', compact('data','message','media','selectedCategories'));
      }
      if($type=="Product"){
        if ($data->mediaType=='Image') {
          $filename='product/images/'.$data->mediaPath;
        }
        else{
          $filename='product/videos/'.$data->mediaPath;
        }
        File::delete($filename);
        $data->delete();
        $message="One data deleted";
        $data = Product::findOrFail($otherid);
        $media = Media::where('productId',$otherid)->get();
        $selectedCategories = \App\AssetCategory::where('productId', $otherid)->get();
        return view('product.edit', compact('data','message','media','selectedCategories'));
      }
    }else{
      return View('auth.login');
    }
  }
}
