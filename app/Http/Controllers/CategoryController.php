<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
Use \App\Category;

class CategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Category::all();
        return view('category.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Category::where('categoryesOfUser','=',$authenticatedUser->id);
        return View('category.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('category.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $category = new \App\Category;
      $category->categoryName = $request->get('categoryName');
      $category->categorystatus = $request->get('categorystatus');
      $category->categoryDescription = $request->get('categoryDescription');
      $category->isPrimary = $request->get('isPrimary');
      //$data= Request::all();
      //$data= Category::create($data);
      $category->save();
      $data = $category;
      $message= "One Data Successfully saved";
      return View('category.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Category::findOrFail($id);
      $message= "One data Requested";
      return View('category.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Category::findOrFail($id);
    $message= "One data Requested";
    return view('category.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $category = Category::findOrFail($id);
    //$input = Request::all();
    //$data->update($input);
    $category->categoryName = $request->get('categoryName');
    $category->categorystatus = $request->get('categorystatus');
    $category->categoryDescription = $request->get('categoryDescription');
    $category->isPrimary = $request->get('isPrimary');
    $category->save();
    $data=$category;
    $message="One data updated";
    return view('category.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Category::findOrFail($id);
    $data->delete();
    $subcategory= \App\Subcategory::where('childCategory',$id)->orWhere('parentCategory', $id)->get();
    foreach ($subcategory as $key => $value) {
      $value->delete();
    }
    $data= Category::all();
    $message="One data deleted";
    return view('category.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }
}
