<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\User;
use \App\Contact;

class ContactController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Contact::all();
        return view('contact.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Contact::where('contactesOfUser','=',$authenticatedUser->id);
        return View('contact.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('contact.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $data= new Contact;
      $data->contactsOf=$request->get('contactsOf');
      $data->contactsBody=$request->get('contactsBody');
      $data->contactsOfUser=$request->get('contactsOfUser');
      $data->save();

      $data= User::findOrFail($request->contactsOfUser);
      $message= "One Data Successfully saved";
      return redirect()->back()->with('message','data');
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Contact::findOrFail($id);
      $message= "One data Requested";
      return view('contact.show',compact('date','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Contact::findOrFail($id);
    $message= "One data Requested";
    return view('contact.edit', compact('$data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = Contact::findOrFail($id);
    $input = Request::all();
    $data->update($input);
    $message="One data updated";
    return view('contact.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request,$id)
  {

    if(Auth::User()){
    $data = Contact::findOrFail($id);
    $data->delete();
    $data= User::findOrFail($request->ofUser);
    $message= "One Data Successfully Deleted";
    return redirect()->back()->with('message','data');
    }else{
      return View('auth.login');
    }
  }
}
