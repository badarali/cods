<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class AssetBidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('assetbid.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assetbid.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assetBid = new \App\AssetBid;
        $assetBid->baseAmount = $request->get('baseAmount');
        //$assetBid->time = $request->get('time');
        if($request->get('productId') != null){
            $assetBid->productId = $request->get('productId');
        }
        else if($request->get('dealId') != null){
          $assetBid->dealId = $request->get('dealId');
        }
        $assetBid->sellerId = Auth::id();
        $assetBid->status = $request->get('status');
        $startTime = Carbon::now();
        $endTime = Carbon::now();
        $endTime = $endTime->addMinutes($request->get('time'));
        //echo($startTime." ".$endTime." ".$request->get('time'));
        //dd($endTime);
        $assetBid->startTime = $startTime;
        $assetBid->endTime = $endTime;
        $assetBid->status = "run";
        $assetBid->save();
        return redirect::to('/assetbid');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $assetbid = \App\AssetBid::findOrFail($id);
      $data= \App\Product::findOrFail($assetbid->productId);
      $productId = $data->id;
      $message= "One data Requested";
      $assetcategories = \App\AssetCategory::where('productId', $productId)->get();
      foreach ($assetcategories as $key => $value) {
        if ($key==0) {
          $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
        }
        else{
          $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          $categories = $categories->merge($result);
        }
      }
      $count=0;
      $similarProducts=\App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$productId)->get();;
      foreach ($categories as $key => $value) {
        if ($value->productId!=$productId) {
          $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$productId)->get();
          if ($count==0) {
            $similarProducts = $result;
            $count++;
          }
          else{
            $similarProducts = $similarProducts->merge($result);
          }
        }
      }

      $products = array();
      $productids = array();
      if (!$similarProducts->isEmpty()) {
        foreach ($similarProducts as $key => $value) {
          if (!in_array($value->productId, $productids)) {
            $productids[] = $value->productId;
            $products[] = \App\Product::findOrFail($value->productId);
          }
        }
      }
      return view('assetbid.show',compact('data','message', 'products', 'assetbid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assetbid = \App\AssetBid::findOrFail($id);
        $assetbid->delete();
        $bids = \App\Bid::where('assetsBidId', $id)->get();
        foreach ($bids as $key => $value) {
          $value->delete();
        }
        return redirect::to('/assetbid');
    }

    public function websiteBids(){
      return view('websiteBids');
    }
}
