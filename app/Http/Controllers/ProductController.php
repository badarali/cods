<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Product;
use Illuminate\Support\Facades\Input;
use \App\Media;
use \App\AssetCategory;
use File;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if (Auth::User()) {
      if(Auth::User()->role == "Super"){
        $message= "Showing all data";
        $data= Product::all();
        return view('product.index', compact('data','message'));
      }
      if(Auth::User()->role == "Admin"){
        $data= Product::where('productOwner',Auth::id())->get();
        $message= "Showing all data";
        return View('product.index', compact('data','message'));
      }
      else{
        return redirect::to('/home');
      }
    }
    else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      $message= "Form is ready to upload product";
      return View('product.create',compact('message'));
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    if(Auth::User()){
      $data= new \App\Product;
      $data->productName = $request->get('productName');
      $data->productDescriptionPath = $request->get('productDescriptionPath');
      $data->productUnitPrice = $request->get('productUnitPrice');
      $data->productUnitDiscount = $request->get('productUnitDiscount');
      $data->productOwner = $request->get('productOwner');
      $data->productTotalPrice = $request->get('productTotalPrice');
      $data->productStatus = $request->get('productStatus');
      $data->productVisits = 0;
      $data->save();

      $categories = $request->get('categoryIds');
      foreach ($categories as $key => $value) {
        $productCategory = new \App\AssetCategory;
        $productCategory->categoryId = $value;
        $productCategory->productId = $data->id;
        $productCategory->save();
      }

      if($request->hasFile('files')){
        //$files=$request->file('files');
        $files=Input::file("files");
        $allowedImagefileExtension=['pdf','jpg','png','jpeg'];
        $destinationImages='product/images';
        $destinationVideo='product/videos';
        $allowedVideofileExtension=['mp4','flv','m4p','.mpg','.mpeg','.webm','.mov'];
        foreach ($files as $file) {
          $filename = time() . '-' .$file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          if($checkImage){

            $file->move($destinationImages,$filename);
            $media=new Media();
            $media->mediaType="Image";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->mediasstatus="Active";
            $media->productId=$data->id;
            $media->save();

            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }if($checkVideo){

            $file->move($destinationVideo,$filename);
            $media=new Media();
            $media->mediaType="Video";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->productId=$data->id;
            $media->save();

            // $data= Product::create($request->all());
            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }else{
              $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
          }
        }
      }
      if(Auth::User()->role == "Super"){
        $data= Product::all();
      }
      else if(Auth::User()->role == "Admin"){
        $data= Product::where('productOwner',Auth::id())->get();
      }
      $message= "One Data Successfully saved";
      return View('product.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $data= \App\Product::findOrFail($id);
      $message = "One data Requested";
      $assetcategories = \App\AssetCategory::where('productId', $id)->get();
      foreach ($assetcategories as $key => $value) {
        if ($key==0) {
          $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
        }
        else{
          $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          $categories = $categories->merge($result);
        }
      }
      $count=0;
      $similarProducts=\App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$id)->get();;
      foreach ($categories as $key => $value) {
        if ($value->productId!=$id) {
          $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$id)->get();
          if ($count==0) {
            $similarProducts = $result;
            $count++;
          }
          else{
            $similarProducts = $similarProducts->merge($result);
          }
        }
      }

      $products = array();
      $productids = array();
      if (!$similarProducts->isEmpty()) {
        foreach ($similarProducts as $key => $value) {
          if (!in_array($value->productId, $productids)) {
            $productids[] = $value->productId;
            $products[] = \App\Product::findOrFail($value->productId);
          }
        }
      }
      return view('product.show',compact('data','message', 'products'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Product::findOrFail($id);
    $media = Media::where('productId',$id)->get();
    $selectedCategories = \App\AssetCategory::where('productId', $id)->get();
    $message= "One data Requested";
    return view('product.edit', compact('data','message','media','selectedCategories'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
      $data = Product::findOrFail($id);
      //$input = Request::all();
      //$data->update($input);
      $data->productName = $request->get('productName');
      $data->productDescriptionPath = $request->get('productDescriptionPath');
      $data->productUnitPrice = $request->get('productUnitPrice');
      $data->productUnitDiscount = $request->get('productUnitDiscount');
      $data->productOwner = $request->get('productOwner');
      $data->productTotalPrice = $request->get('productTotalPrice');
      $data->productStatus = $request->get('productStatus');
      $data->save();

      $categories = $request->get('categoryIds');
      $productCategory = \App\AssetCategory::where('productId',$id)->get();
      foreach ($productCategory as $key => $value) {
        $value->delete();
      }

      foreach ($categories as $key => $value) {
        $productCategory = new \App\AssetCategory;
        $productCategory->categoryId = $value;
        $productCategory->productId = $data->id;
        $productCategory->save();
      }

      if($request->hasFile('files')){
        //$files=$request->file('files');
        $files=Input::file("files");
        $allowedImagefileExtension=['pdf','jpg','png','jpeg'];
        $destinationImages='product/images';
        $destinationVideo='product/videos';
        $allowedVideofileExtension=['mp4','flv','m4p','.mpg','.mpeg','.webm','.mov'];
        foreach ($files as $file) {
          $filename = time() . '-' .$file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          if($checkImage){

            $file->move($destinationImages,$filename);
            $media=new Media();
            $media->mediaType="Image";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->mediasstatus="Active";
            $media->productId=$data->id;
            $media->save();

            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }if($checkVideo){

            $file->move($destinationVideo,$filename);
            $media=new Media();
            $media->mediaType="Video";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->productId=$data->id;
            $media->save();

            // $data= Product::create($request->all());
            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }else{
              $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
          }
        }
      }
      if(Auth::User()->role == "Super"){
        $data= Product::all();
      }
      else if(Auth::User()->role == "Admin"){
        $data= Product::where('productOwner',Auth::id())->get();
      }
      $message="One data updated";
      return view('product.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Product::findOrFail($id);
    $data->delete();
    $data = \App\Media::where('productId', $id)->get();

    $productCategory = \App\AssetCategory::where('productId',$id)->get();
    foreach ($productCategory as $key => $value) {
      $value->delete();
    }

    foreach ($data as $key => $value) {
      if ($value->mediaType=='Image') {
        $filename='product/images/'.$value->mediaPath;
      }
      else{
        $filename='product/videos/'.$value->mediaPath;
      }
      File::delete($filename);
      $value->delete();
    }
    if(Auth::User()->role == "Super"){
      $data= Product::all();
    }
    else if(Auth::User()->role == "Admin"){
      $data= Product::where('productOwner',Auth::id())->get();
    }
    $message="One data deleted";
    return View('product.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  public function productsInCategory($category, $id){
    $assets = \App\AssetCategory::where('categoryId', $id)->get();
    $products = array();
    foreach ($assets as $key => $value) {
      if (!empty($value->productId)) {
        $products[] = \App\Product::findOrFail($value->productId);
      }
    }
    $message = "Products in $category";
    return View('productsInCategory', compact('products', 'message', 'category', 'id'));
  }
}
