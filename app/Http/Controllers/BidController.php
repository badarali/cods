<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('bid.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bid.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $checkBids = \App\Bid::where('bidAmount', $request->get('bidAmount'))->where('assetsBidId', $request->get('assetBidId'))->get();
      if($checkBids->isEmpty()){
        $bid = new \App\Bid;
        $bid->bidAmount = $request->get('bidAmount');
        $bid->assetsBidId = $request->get('assetBidId');
        $bid->byUserId = Auth::id();
        $bid->status = "run";
        $bid->save();
        $url = "/assetbid/".$request->get('assetBidId');
        return redirect::to($url);
      }
      else{
        $message = "This amount has already been bid";
        $url = "/assetbid/".$request->get('assetBidId');
        return redirect::to($url);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $assetbid = \App\AssetBid::findOrFail($id);
      $data= \App\Product::findOrFail($assetbid->productId);
      $productId = $data->id;
      $message= "One data Requested";
      $assetcategories = \App\AssetCategory::where('productId', $productId)->get();
      foreach ($assetcategories as $key => $value) {
        if ($key==0) {
          $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
        }
        else{
          $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          $categories = $categories->merge($result);
        }
      }
      $count=0;
      $similarProducts=\App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$productId)->get();;
      foreach ($categories as $key => $value) {
        if ($value->productId!=$productId) {
          $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$productId)->get();
          if ($count==0) {
            $similarProducts = $result;
            $count++;
          }
          else{
            $similarProducts = $similarProducts->merge($result);
          }
        }
      }

      $products = array();
      $productids = array();
      if (!$similarProducts->isEmpty()) {
        foreach ($similarProducts as $key => $value) {
          if (!in_array($value->productId, $productids)) {
            $productids[] = $value->productId;
            $products[] = \App\Product::findOrFail($value->productId);
          }
        }
      }
      return view('bid.show',compact('data','message', 'products', 'assetbid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bid = \App\Bid::findOrFail($id);
        $bid->delete();
        return redirect::to('/bid');
    }

    public function websiteBids(){
      $now = new Carbon();
      $data = \App\AssetBid::where('status', 'run')->get();
      return view('websiteBids', compact('now', 'data'));
    }

    public function getBids($assetbid_id){
      $bids = \App\Bid::where('assetsBidId', $assetbid_id)->get();
      $comments = \App\Livecomment::where('assetBidId', $assetbid_id)->get();
      $userCommented = [];
      $users = [];

      foreach ($bids as $key => $value) {
        $users[] = \App\User::findOrFail($value->byUserId);
      }

      foreach ($comments as $key => $value) {
        $userCommented[] = \App\User::findOrFail($value->byUserId);
      }
      $assetbid = \App\AssetBid::findOrFail($assetbid_id);
      $now = new Carbon();
      $endTime = new Carbon($assetbid->endTime);
      $remain = $now->diff($endTime);
      $remainTime = $remain->format('%h:%i:%s');
      $expireCheck = false;
      if ($now > $assetbid->endTime) {
        $expireCheck = true;
        $count = $bids->count();
        if($count == 0){
            $assetbid->status = "loss";
            $assetbid->save();
        }
        else{
            $highestBid = \App\Bid::where('assetsBidId', $assetbid_id)->orderBy('bidAmount', 'desc')->first();
            $highestBid->status = "win";
            $highestBid->save();
            $assetbid->status = "win";
            $assetbid->save();
        }
      }
      $iframe = Auth::User()->googleMapsLink;
      return response()->json([
        'bids' => $bids,
        'users' => $users,
        'isExpire' => $expireCheck,
        'remaining' => $remainTime,
        'comments' => $comments,
        'userCommented' => $userCommented,
        'iframe' => $iframe,
      ]);
    }

    public function getBidsOfAsset($assetbid_id){
      return view('assetbid.details', compact('assetbid_id'));
    }
}
