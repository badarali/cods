<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Deal;
use Illuminate\Support\Facades\Input;
use \App\Media;
use File;
use Illuminate\Support\Facades\Redirect;
class DealController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
    if (Auth::User()) {
      if($authenticatedUser->role == 'Super'){
        $data= Deal::all();
        return view('deal.index', compact('data','message'));
      }
      if($authenticatedUser->role == 'Admin'){
        $data= Deal::where('dealOwner',$authenticatedUser->id)->get();
        return View('deal.index', compact('data','message'));
      }
      else{
        return redirect::to('/websiteDeals');
      }
    }
    else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('deal.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $data= new \App\Deal;
      $data->dealName = $request->get('dealName');
      $data->dealDescriptionPath = $request->get('dealDescriptionPath');
      $data->dealUnitPrice = $request->get('dealUnitPrice');
      $data->dealUnitDiscount = $request->get('dealUnitDiscount');
      $data->dealOwner = $request->get('dealOwner');
      $data->dealTotalPrice = $request->get('dealTotalPrice');
      $data->dealStatus = $request->get('dealStatus');
      $data->dealVisits = 0;
      $data->save();

      $categories = $request->get('categoryIds');
      foreach ($categories as $key => $value) {
        $productCategory = new \App\AssetCategory;
        $productCategory->categoryId = $value;
        $productCategory->dealId = $data->id;
        $productCategory->save();
      }

      if($request->hasFile('files')){
        //$files=$request->file('files');
        $files=Input::file("files");
        $allowedImagefileExtension=['pdf','jpg','png','jpeg'];
        $destinationImages='deal/images';
        $destinationVideo='deal/videos';
        $allowedVideofileExtension=['mp4','flv','m4p','.mpg','.mpeg','.webm','.mov'];
        foreach ($files as $file) {
          $filename = time() . '-' .$file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $checkVideo=in_array($extension,$allowedVideofileExtension);

          if($checkImage){
            $file->move($destinationImages,$filename);
            $media=new Media();
            $media->mediaType="Image";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->mediasstatus="Active";
            $media->dealId=$data->id;
            $media->save();

            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }if($checkVideo){

            $file->move($destinationVideo,$filename);
            $media=new Media();
            $media->mediaType="Video";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->dealId=$data->id;
            $media->save();

            // $data= Product::create($request->all());
            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }else{
              $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
          }
        }
      }
      if (Auth::User()->role=='Super') {
        $data= Deal::all();
      }
      else if (Auth::User()->role=='Admin') {
        $data= Deal::where('dealOwner',Auth::id())->get();
      }
      $message= "One Data Successfully saved";
      return View('deal.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

      $data= Deal::findOrFail($id);
      $message= "One data Requested";
      $assetcategories = \App\AssetCategory::where('dealId', $id)->get();
      foreach ($assetcategories as $key => $value) {
        if ($key==0) {
          $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
        }
        else{
          $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          $categories = $categories->merge($result);
        }
      }
      $similarDeals = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$id)->get();
      $count=0;
      foreach ($categories as $key => $value) {
        if ($value->dealId!=$id) {
          $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$id)->get();
          if ($count==0) {
            $similarDeals = $result;
            $count++;
          }
          else{
            $similarDeals = $similarDeals->merge($result);
          }
        }
      }
      $deals = array();
      $dealids = array();
      if (!$similarDeals->isEmpty()) {
        foreach ($similarDeals as $key => $value) {
          if (!in_array($value->dealId, $dealids)) {
            $dealids[] = $value->dealId;
            $deals[] = \App\Deal::findOrFail($value->dealId);
          }
        }
      }
      return view('deal.show',compact('data','message', 'deals'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Deal::findOrFail($id);
    $media = Media::where('dealId',$id)->get();
    $selectedCategories = \App\AssetCategory::where('dealId', $id)->get();
    $message= "One data Requested";
    return view('deal.edit', compact('data','message','media', 'selectedCategories'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
      $data = Deal::findOrFail($id);
      //$input = Request::all();
      //$data->update($input);
      $data->dealName = $request->get('dealName');
      $data->dealDescriptionPath = $request->get('dealDescriptionPath');
      $data->dealUnitPrice = $request->get('dealUnitPrice');
      $data->dealUnitDiscount = $request->get('dealUnitDiscount');
      $data->dealOwner = $request->get('dealOwner');
      $data->dealTotalPrice = $request->get('dealTotalPrice');
      $data->dealStatus = $request->get('dealStatus');
      $data->save();

      $categories = $request->get('categoryIds');
      $dealCategory = \App\AssetCategory::where('dealId',$id)->get();
      foreach ($dealCategory as $key => $value) {
        $value->delete();
      }

      foreach ($categories as $key => $value) {
        $dealCategory = new \App\AssetCategory;
        $dealCategory->categoryId = $value;
        $dealCategory->dealId = $data->id;
        $dealCategory->save();
      }

      if($request->hasFile('files')){
        //$files=$request->file('files');
        $files=Input::file("files");
        $allowedImagefileExtension=['pdf','jpg','png','jpeg'];
        $destinationImages='deal/images';
        $destinationVideo='deal/videos';
        $allowedVideofileExtension=['mp4','flv','m4p','.mpg','.mpeg','.webm','.mov'];
        foreach ($files as $file) {
          $filename = time() . '-' .$file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          if($checkImage){

            $file->move($destinationImages,$filename);
            $media=new Media();
            $media->mediaType="Image";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->mediasstatus="Active";
            $media->dealId=$data->id;
            $media->save();

            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }if($checkVideo){

            $file->move($destinationVideo,$filename);
            $media=new Media();
            $media->mediaType="Video";
            $media->mediaExtension=$extension;
            $media->mediaPath=$filename;
            $media->dealId=$data->id;
            $media->save();

            // $data= Product::create($request->all());
            // $data->productDescriptionPath=$destinationImages;
            // $data->save();
          }else{
              $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
          }
        }
      }
      if (Auth::User()->role=='Super') {
        $data= Deal::all();
      }
      else if (Auth::User()->role=='Admin') {
        $data= Deal::where('dealOwner',Auth::id())->get();
      }
      $message="One data updated";
      return view('deal.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Deal::findOrFail($id);
    $data->delete();
    $data = \App\Media::where('dealId', $id)->get();

    $dealCategory = \App\AssetCategory::where('dealId',$id)->get();
    foreach ($dealCategory as $key => $value) {
      $value->delete();
    }

    foreach ($data as $key => $value) {
      if ($value->mediaType=='Image') {
        $filename='deal/images/'.$value->mediaPath;
      }
      else{
        $filename='deal/videos/'.$value->mediaPath;
      }
      File::delete($filename);
      $value->delete();
    }
    if (Auth::User()->role=='Super') {
      $data= Deal::all();
    }
    else if (Auth::User()->role=='Admin') {
      $data= Deal::where('dealOwner',Auth::id())->get();
    }
    $message="One data deleted";
    return View('deal.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  public function dealsInCategory($category, $id){
    $assets = \App\AssetCategory::where('categoryId', $id)->get();
    $deals = array();
    foreach ($assets as $key => $value) {
      if (!empty($value->dealId)) {
        $deals[] = \App\Deal::findOrFail($value->dealId);
      }
    }
    $message = "Deals in $category";
    return View('dealsInCategory', compact('deals', 'message', 'category', 'id'));
  }
}
