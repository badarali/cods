<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SupportController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Support::all();
        return view('support.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Support::where('supportesOfUser','=',$authenticatedUser->id);
        return View('support.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('support.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $data= Request::all();
      $data= Support::create($data);
      $message= "One Data Successfully saved";
      return View('support.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Support::findOrFail($id);
      $message= "One data Requested"
      return view('support.show',compact('date','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Support::findOrFail($id);
    $message= "One data Requested"
    return view('support.edit', compact('$data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = Support::findOrFail($id);
    $input = Request::all();
    $data->update($input);
    $message="One data updated";
    return view('support.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Support::findOrFail($id);
    $data->delete();
    $message="One data deleted"
    return redirect('support.index',compact('message'));
    }else{
      return View('auth.login');
    }
  }
}
