<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Address;
use \App\User;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $authenticatedUser=Auth::User();
      $message= "Showing all data";
        if($authenticatedUser->role == 'Super'){
          $data= Address::all();
          return view('address.index', compact('data','message'));
        }if($authenticatedUser->role != 'Super'){
          $data= Address::where('addressesOfUser','=',$authenticatedUser->id);
          return View('user.show', compact('data','message'));
        }else{
          return View('auth.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::User()){
        return View('address.create');
      }else{
        return View('auth.login');
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::User()){
        $data= new Address;
        $data->addressesOf=$request->get('addressesOf');
        $data->addressesBody=$request->get('addressesBody');
        $data->addressesOfUser=$request->get('addressesOfUser');
        $data->save();

        $data= User::findOrFail($request->addressesOfUser);
        $message= "One Data Successfully saved";
        return redirect()->back()->with('message','data');
      }else{
        return View('auth.login');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(Auth::User()){
        $data= Address::findOrFail($id);
        $message= "One data Requested";
        return view('address.show',compact('date','message'));
      }else{
        return View('auth.login');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::User()){
      $data = Address::findOrFail($id);
      $message= "One data Requested";
      return view('address.edit', compact('$data','message'));
      }else{
        return View('auth.login');

      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(Auth::User()){
      $data = Address::findOrFail($id);
      $input = Request::all();
      $data->update($input);
      $message="One data updated";
      return view('address.show',compact('data','message'));
      }else{
        return View('auth.login');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request,$id)
     {

       if(Auth::User()){
       $data = Address::findOrFail($id);
       $data->delete();
       $data= User::findOrFail($request->ofUser);
       $message= "One Data Successfully Deleted";
       return redirect()->back()->with('message','data');
       }else{
         return View('auth.login');
       }
     }
}
