<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Deal;
use Illuminate\Support\Facades\Input;
use \App\Media;
use Illuminate\Support\Facades\Redirect;
use \App\Product;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Auth::User() && Auth::User()->profileStatus=='Active') {
        return view('welcome');
      }
      else if(!Auth::User())
      {
        return view('welcome');
      }
      else{
        return redirect::to('/logout');
      }
    }

    public function dashboard(){
      return view('dashboard');
    }

    public function single($id){
      $data= \App\Product::findOrFail($id);
      return view('single',compact('data'));
    }

    public function websitedeal(){
      return view('websitedeals');
    }

    public function cart(){
      return view('cart');
    }

}
