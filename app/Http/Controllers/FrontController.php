<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Redirect;
use Mail;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function create(){
      if(Auth::User()->role=='Super'){
        $frontCount = \App\Front::count();
        if ($frontCount>0) {
          return redirect::to('/home');
        }
        else if($frontCount==0){
          return View('frontWeb.create');
        }
      }
      else{
        return redirect::to('/home');
      }
    }

    public function store(Request $request)
    {
      if(Auth::User()->role=='Super'){
        $front = new \App\Front;
        $front->googlePlus = $request->get('googlePlus');
        $front->facebook = $request->get('facebook');
        $front->twitter = $request->get('twitter');
        $front->linkedin = $request->get('linkedin');
        $front->youtube = $request->get('youtube');
        $front->instagram = $request->get('instagram');
        $front->contact = $request->get('contact');
        $front->email = $request->get('email');
        $front->address = $request->get('address');
        if($request->hasFile('logo')){
          //$files=$request->file('files');
          $file=Input::file("logo");
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $destinationImages='logo/images';
          $filename = time() . '-' .$file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          if($checkImage){
            $file->move($destinationImages,$filename);
            $front->logoPath = $filename;
            $front->save();
            $message= "One Data Successfully saved";
            return redirect::to('/home');
          }
          else
          {
            $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
          }
        }
        $front->save();
      }
      else{
        return redirect::to('/home');
      }
    }

    public function edit($id)
    {
      if(Auth::User()->role=='Super'){
      $front = \App\Front::findOrFail($id);
      $message= "One data Requested";
      return view('frontWeb.edit', compact('front','message'));
      }
      else{
        return redirect::to('/home');
      }
    }

    public function update(Request $request, $id)
    {
      if(Auth::User()->role=='Super'){
        $front = \App\Front::findOrFail($id);
        $front->googlePlus = $request->get('googlePlus');
        $front->facebook = $request->get('facebook');
        $front->twitter = $request->get('twitter');
        $front->youtube = $request->get('youtube');
        $front->linkedin = $request->get('linkedin');
        $front->instagram = $request->get('instagram');
        $front->contact = $request->get('contact');
        $front->email = $request->get('email');
        $front->address = $request->get('address');
        if($request->hasFile('logo')){
          $data = \App\Front::findOrFail($id);
          if (!empty($data->logoPath)) {
            $filename='logo/images/'.$data->logoPath;
            File::delete($filename);
          }
          $file=Input::file("logo");
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $destinationImages='logo/images';
          $filename = time() . '-' .$file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          if($checkImage){
            $file->move($destinationImages,$filename);
            $front->logoPath = $filename;
            $front->save();
            $message= "One Data Successfully saved";
            return redirect::to('/home');
          }
          else
          {
            $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
          }
        }
        $front->save();
        return redirect::to('/home');
      }
      else{
        return redirect::to('/home');
      }
    }

    public function contact(){
      return View('contact');
    }

    public function sendMail(Request $request){
      $email = $request->get('email');
      $name = $request->get('name');
      $subject = $request->get('subject');
      $message1 = $request->get('message');
      $data = array('name'=>"Virat Gandhi");
      $to = \App\Front::first();
      $to = $to->email;
      Mail::send('mail', ['name'=>$name,'email'=>$email,'message1'=>$message1], function($message) use($to, $email, $name, $subject) {
         $message->to('sajidsalman75@gmail.com', 'Contact Mail')->subject
            ($subject);
         $message->from('sajidsalman75@gmail.com','COD');
      });
      return redirect::to('/home');
    }
}
