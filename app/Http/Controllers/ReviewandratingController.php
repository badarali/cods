<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Deal;
use Illuminate\Support\Facades\Input;
use \App\Product;

class ReviewandratingController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all Reviews/Ratings";
      if($authenticatedUser->role == 'Super'){
        $data= \App\Reviewandrating::all();
        return view('reviewandrating.index', compact('data','message'));
      }
      elseif($authenticatedUser->role == 'Admin'){
        return View('dashboard');
      }
      elseif ($authenticatedUser->role == 'Client') {
        $orders = \App\Order::where('orderByUserId', Auth::id())->where('orderStatus', 'Complete')->get();

        $orderMetas = array();
        $products = array();
        $deals = array();
        foreach ($orders as $key => $value) {
          $orderMetas = \App\Ordermeta::where('orderId', $value->id)->get();
          foreach ($orderMetas as $key => $meta) {
            if (!empty($meta->productId)) {
              $reviewCheck = \App\Reviewandrating::where('toProductId', $meta->productId)->where('byUserId', Auth::id())->get();
              if ($reviewCheck->isEmpty()) {
                $products[] = \App\Product::findOrFail($meta->productId);
              }
            }
            else{
              $reviewCheck = \App\Reviewandrating::where('todealId', $meta->dealId)->where('byUserId', Auth::id())->get();
              if ($reviewCheck->isEmpty()) {
                $deals[] = \App\Deal::findOrFail($meta->dealId);
              }
            }
          }
        }
        return View('reviewandrating.index', compact('deals','message', 'products'));
      }
      else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('reviewandrating.create');
    }else{
      return View('auth.login');
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $rate = new \App\Reviewandrating;
      $rate->stars = $request->get('rating');
      $rate->reviewPath = $request->get('review');
      if($request->get('type')=='Deal'){
        $rate->toDealId = $request->get('toDealId');
      }
      else if($request->get('type')=='Product'){
        $rate->toProductId = $request->get('toProductId');
      }
      $rate->byUserId = Auth::id();
      $rate->status = 'Active';
      $rate->save();
      $message= "One Data Successfully saved";
      if($request->get('type')=='Deal'){
        $data= \App\Deal::findOrFail($request->get('toDealId'));
        $assetcategories = \App\AssetCategory::where('dealId', $request->get('toDealId'))->get();
        foreach ($assetcategories as $key => $value) {
          if ($key==0) {
            $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          }
          else{
            $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            $categories = $categories->merge($result);
          }
        }
        $count=0;
        foreach ($categories as $key => $value) {
          if ($value->dealId!=$request->get('toDealId')) {
            $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$request->get('toDealId'))->get();
            if ($count==0) {
              $similarDeals = $result;
              $count++;
            }
            else{
              $similarDeals = $similarDeals->merge($result);
            }
          }
        }

        $deals = array();
        $dealids = array();
        if (!$similarDeals->isEmpty()) {
          foreach ($similarDeals as $key => $value) {
            if (!in_array($value->dealId, $dealids)) {
              $dealids[] = $value->dealId;
              $deals[] = \App\Deal::findOrFail($value->dealId);
            }
          }
        }
        return View('deal.show',compact('data','message','deals'));
      }
      else if($request->get('type')=='Product'){
        $data= \App\Product::findOrFail($request->get('toProductId'));
        $assetcategories = \App\AssetCategory::where('productId', $request->get('toProductId'))->get();
        foreach ($assetcategories as $key => $value) {
          if ($key==0) {
            $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          }
          else{
            $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            $categories = $categories->merge($result);
          }
        }
        $count=0;
        $similarProducts = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$request->get('toProductId'))->get();
        foreach ($categories as $key => $value) {
          if ($value->productId!=$request->get('toProductId')) {
            $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$request->get('toProductId'))->get();
            if ($count==0) {
              $similarProducts = $result;
              $count++;
            }
            else{
              $similarProducts = $similarProducts->merge($result);
            }
          }
        }

        $products = array();
        $productids = array();
        if (!$similarProducts->isEmpty()) {
          foreach ($similarProducts as $key => $value) {
            if (!in_array($value->productId, $productids)) {
              $productids[] = $value->productId;
              $products[] = \App\Product::findOrFail($value->productId);
            }
          }
        }
        return View('product.show',compact('data','message','products'));
      }
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= \App\Reviewandrating::findOrFail($id);
      $message= "One data Requested";
      return view('reviewandrating.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Reviewandrating::findOrFail($id);
    $message= "One data Requested";
    return view('reviewandrating.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = \App\Reviewandrating::findOrFail($id);
    $input = Request::all();
    $data->update($input);
    $message="One data updated";
    return view('reviewandrating.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = \App\Reviewandrating::findOrFail($id);
    $data->delete();
    $message="One data deleted";
    $data= \App\Reviewandrating::all();
    return View('reviewandrating.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  public function reviewrate($id, $type){
    if (Auth::User()->role=='Client') {
      if ($type=='deal') {
        $data= \App\Deal::findOrFail($id);
        $message= "Rate";
        $assetcategories = \App\AssetCategory::where('dealId', $id)->get();
        foreach ($assetcategories as $key => $value) {
          if ($key==0) {
            $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          }
          else{
            $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            $categories = $categories->merge($result);
          }
        }
        $count=0;
        foreach ($categories as $key => $value) {
          if ($value->dealId!=$id) {
            $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('dealId','!=',$id)->get();
            if ($count==0) {
              $similarDeals = $result;
              $count++;
            }
            else{
              $similarDeals = $similarDeals->merge($result);
            }
          }
        }

        $deals = array();
        $dealids = array();
        if (!$similarDeals->isEmpty()) {
          foreach ($similarDeals as $key => $value) {
            if (!in_array($value->dealId, $dealids)) {
              $dealids[] = $value->dealId;
              $deals[] = \App\Deal::findOrFail($value->dealId);
            }
          }
        }
        return view('deal.show',compact('data','message','deals'));
      }
      else if($type=='product'){
        $data= \App\Product::findOrFail($id);
        $message= "Rate";
        $assetcategories = \App\AssetCategory::where('productId', $id)->get();
        foreach ($assetcategories as $key => $value) {
          if ($key==0) {
            $categories = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
          }
          else{
            $result = \App\AssetCategory::where('categoryId',$value->categoryId)->get();
            $categories = $categories->merge($result);
          }
        }
        $count=0;
        $similarProducts = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$id)->get();;
        foreach ($categories as $key => $value) {
          if ($value->productId!=$id) {
            $result = \App\AssetCategory::where('categoryId', $value->categoryId)->where('productId','!=',$id)->get();
            if ($count==0) {
              $similarProducts = $result;
              $count++;
            }
            else{
              $similarProducts = $similarProducts->merge($result);
            }
          }
        }

        $products = array();
        $productids = array();
        if (!$similarProducts->isEmpty()) {
          foreach ($similarProducts as $key => $value) {
            if (!in_array($value->productId, $productids)) {
              $productids[] = $value->productId;
              $products[] = \App\Product::findOrFail($value->productId);
            }
          }
        }
        return view('product.show',compact('data','message', 'products'));
      }
    }
  }
}
