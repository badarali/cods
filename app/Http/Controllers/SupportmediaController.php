<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SupportmediaController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Supportmedia::all();
        return view('supportmedia.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Supportmedia::where('supportmediaesOfUser','=',$authenticatedUser->id);
        return View('supportmedia.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('supportmedia.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $data= Request::all();
      $data= Supportmedia::create($data);
      $message= "One Data Successfully saved";
      return View('supportmedia.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Supportmedia::findOrFail($id);
      $message= "One data Requested"
      return view('supportmedia.show',compact('date','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Supportmedia::findOrFail($id);
    $message= "One data Requested"
    return view('supportmedia.edit', compact('$data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = Supportmedia::findOrFail($id);
    $input = Request::all();
    $data->update($input);
    $message="One data updated";
    return view('supportmedia.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Supportmedia::findOrFail($id);
    $data->delete();
    $message="One data deleted"
    return redirect('supportmedia.index',compact('message'));
    }else{
      return View('auth.login');
    }
  }
}
