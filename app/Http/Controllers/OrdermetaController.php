<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
class OrdermetaController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
    $data = array();
    if($authenticatedUser->role == 'Super'){
      $data= \App\Orderconnect::all();
      return view('ordermeta.index', compact('data','message'));
    }
    else if($authenticatedUser->role == 'Admin'){
      $orders= \App\Order::where('orderToSellerId',Auth::id())->get();
      foreach ($orders as $key => $value) {
        $data[] = \App\Orderconnect::where('id', $value->orderconnectId)->first();
      }
      return View('ordermeta.index', compact('data','message'));
    }
    elseif ($authenticatedUser->role == 'Client') {
      $orders= \App\Order::where('orderByUserId',Auth::id())->get();
      $connectids = array();
      foreach ($orders as $key => $value) {
        $connect = \App\Orderconnect::where('id', $value->orderconnectId)->first();
        if (sizeof($connectids) == 0) {
          $data[] = \App\Orderconnect::where('id', $value->orderconnectId)->first();
          $connectids[] = $connect->id;
        }
        elseif(!in_array($connect->id, $connectids)){
          $data[] = \App\Orderconnect::where('id', $value->orderconnectId)->first();
          $connectids[] = $connect->id;
        }
      }
      return View('ordermeta.index', compact('data','message'));
    }
    else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('ordermeta.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $orderIds = array();
    $ownerIds = array();
    $orderconnect = new \App\Orderconnect;
    $orderconnect->orderStatus = 'Active';
    $orderconnect->save();
    $ids = json_decode($request->get('id'));
    $count = json_decode($request->get('count'));
    $types = json_decode($request->get('type'));
    if(Auth::User()){
      foreach ($ids as $key => $value) {
        $orderMeta = new \App\Ordermeta;
        $orderMeta->orderconnectId = $orderconnect->id;
        $orderMeta->quantity = $count[$key];
        if ($types[$key]=='Product') {
          $product = \App\Product::where('id', $value)->first();
          if(!in_array($product->productOwner, $ownerIds)){
            $order = new \App\Order;
            $order->orderconnectId = $orderconnect->id;
            $order->orderStatus = 'Active';
            $order->orderByUserId = Auth::id();
            $ownerIds[] = $product->productOwner;
            $total = $product->productTotalPrice * $count[$key];
            $order->orderTotalPrice = $order->orderTotalPrice + $total;
            //$order->orderTotalPrice = $order->orderTotalPrice * $count[$key];
            $order->orderCalculatedSum = 0;
            $order->orderToSellerId = $product->productOwner;
            $order->orderOnAddressId = $request->get('address');
            $order->orderOnContactId = $request->get('contact');
            $order->orderInCart = 1;
            $order->save();
            $orderIds[] = $order->id;
          }
          else{
            $index = array_search($product->productOwner, $ownerIds);
            $orderid = $orderIds[$index];
            $order = \App\Order::where('id', $orderid)->first();
            $total = $product->productTotalPrice * $count[$key];
            $order->orderTotalPrice = $order->orderTotalPrice + $total;
            $order->save();
          }
          $orderMeta->orderId = $order->id;
          $orderMeta->productId = $value;
        }
        elseif ($types[$key]=='Deal') {
          $deal = \App\Deal::where('id', $value)->first();
          if(!in_array($deal->dealOwner, $ownerIds)){
            $order = new \App\Order;
            $order->orderconnectId = $orderconnect->id;
            $order->orderStatus = 'Active';
            $order->orderByUserId = Auth::id();
            $ownerIds[] = $deal->dealOwner;
            $total = $deal->dealTotalPrice * $count[$key];
            $order->orderTotalPrice = $order->orderTotalPrice + $total;
            $order->orderCalculatedSum = 0;
            $order->orderToSellerId = $deal->dealOwner;
            $order->orderOnAddressId = $request->get('address');
            $order->orderOnContactId = $request->get('contact');
            $order->orderInCart = 1;
            $order->save();
            $orderIds[] = $order->id;
          }
          else{
            $index = array_search($deal->dealOwner, $ownerIds);
            $orderid = $orderIds[$index];
            $order = \App\Order::where('id', $orderid)->first();
            $total = $deal->dealTotalPrice * $count[$key];
            $order->orderTotalPrice = $order->orderTotalPrice + $total;
            $order->save();
          }
          $orderMeta->orderId = $order->id;
          $orderMeta->dealId = $value;
        }
        $orderMeta->save();
      }
      $message = 'Ordered';
      return view('welcome', compact('message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= \App\Orderconnect::findOrFail($id);
      $message= "One data Requested";
      return view('ordermeta.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()->role == 'Super'){
    $data = \App\Orderconnect::findOrFail($id);
    $message= "One data Requested";
    return view('ordermeta.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = \App\Orderconnect::findOrFail($id);
    $status = $request->get('orderStatus');
    $data->orderStatus = $status;
    $data->save();
    $order = \App\Order::where('orderconnectId', $id)->get();
    foreach ($order as $key => $value) {
      $value->orderStatus = $status;
      $value->save();
    }
    $message="One data updated";
    return view('ordermeta.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Ordermeta::findOrFail($id);
    $data->delete();
    $message="One data deleted";
    return redirect('ordermeta.index',compact('message'));
    }else{
      return View('auth.login');
    }
  }

  public function addressescontacts(Request $request){
    if (Auth::User()) {
      $contact = $request->get('contact');
      $address = $request->get('address');
      $orderids = $request->get('orderIds');
      $orderids = unserialize($orderids);
      foreach ($orderids as $key => $value) {
        $order = \App\Order::where('id', $value)->first();
        $order->orderOnAddressId = $address;
        $order->orderOnContactId = $contact;
        $order->save();
        $message = 'Ordered';
        return View('welcome', compact('message'));
      }
    }
    else{
      return View('auth.login');
    }
  }
}
