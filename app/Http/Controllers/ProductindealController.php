<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Product;
use Illuminate\Support\Facades\Input;
use \App\Media;
use \App\Productindeal;

class ProductindealController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= Productindeal::all();
        return view('productindeals.index', compact('data','message'));
      }if($authenticatedUser->role != 'Super'){
        $data= Productindeal::where('productindealesOfUser','=',$authenticatedUser->id);
        return View('productindeals.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('productindeals.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if(Auth::User()){
      $data= new \App\Productindeal;
      $data->productId = $request->get('productId');
      $data->dealId = $request->get('dealId');
      $data->productindealsstatus = $request->get('productindealsstatus');
      $data->save();
      $data = Productindeal::all();
      $message = "One Data Successfully saved";
      return View('productindeals.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= Productindeal::findOrFail($id);
      $message= "One data Requested";
      return view('productindeals.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = Productindeal::findOrFail($id);
    $message= "One data Requested";
    return view('productindeals.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
      $data = Productindeal::findOrFail($id);
      $data->productId = $request->get('productId');
      $data->dealId = $request->get('dealId');
      $data->productindealsstatus = $request->get('productindealsstatus');
      $data->save();
      $message="One data updated";
      $data = Productindeal::all();
      return view('productindeals.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = Productindeal::findOrFail($id);
    $data->delete();
    $message="One data deleted";
    $data = Productindeal::all();
    return View('productindeals.index',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }
}
