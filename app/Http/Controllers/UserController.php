<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
Use \App\User;
use Illuminate\Support\Facades\Input;
use \App\Media;
use File;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $authenticatedUser=Auth::User();
    $message= "Showing all data";
      if($authenticatedUser->role == 'Super'){
        $data= User::all();
        return view('user.index', compact('data','message'));
      }if($authenticatedUser->role != 'admin'){
        $data= User::where('useresOfUser','=',$authenticatedUser->id);
        // return View('user.index', compact('data','message'));
      }else{
        return View('auth.login');
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    if(Auth::User()){
      return View('user.create');
    }else{
      return View('auth.login');
    }

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    if(Auth::User()){
      $data= Request::all();
      $data= User::create($data);
      $message= "One Data Successfully saved";
      return View('user.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    if(Auth::User()){
      $data= User::findOrFail($id);
      $message= "One data Requested";
      return view('user.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    if(Auth::User()){
    $data = User::findOrFail($id);
    $message= "One data Requested";
    return view('user.edit', compact('data','message'));
    }else{
      return View('auth.login');

    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if(Auth::User()){
    $data = User::findOrFail($id);
    $data->name=$request->get('name');
    $data->profileStatus=$request->get('profileStatus');
    $password = $request->get('password');
    $password = bcrypt($password);
    $data->password = $password;
    $data->update();

    if($request->hasFile('profileImage')){

      $previousImage= Media::where('userProfileId',$id)->get();
      foreach ($previousImage as $key => $value) {
        if ($value->mediaType=='Image') {
          $filename='profile/images/'.$value->mediaPath;
        }
        File::delete($filename);
        $value->delete();
        // $previousImage->delete();
      }




      $file=Input::file("profileImage");
      $allowedImagefileExtension=['pdf','jpg','png','jpeg'];
      $destinationImages='profile/images';
      $allowedVideofileExtension=['mp4','flv','m4p','.mpg','.mpeg','.webm','.mov'];

        $filename = time() . '-' .$file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        if($checkImage){

          $file->move($destinationImages,$filename);
          $media=new Media();
          $media->mediaType="Image";
          $media->mediaExtension=$extension;
          $media->mediaPath=$filename;
          $media->mediasstatus="Active";
          $media->userProfileId=$data->id;
          $media->save();

          // $data->productDescriptionPath=$destinationImages;
          // $data->save();
        }else{
            $message="No data Saved; File Extension for Images 'pdf','jpg','png','jpeg'; for videos 'mp4','flv','m4p','.mpg','.mpeg','.webm','.mov' allowed ";
        }

    }

    $message="One data updated";
    return view('user.show',compact('data','message'));
    }else{
      return View('auth.login');
    }
  }

public function statusNeedChange($id){
  $data = User::findOrFail($id);
  if($data->profileStatus == "Active"){
    $data->profileStatus= "Deactive";
    $data->update();
    $message="One data status deactived";
  }else{
      $data->profileStatus ="Active";
      $data->update();
      $message="One data status activated";
  }
  $data= User::all();
  return view('user.index',compact('data','message'));
}
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(Auth::User()){
    $data = User::findOrFail($id);
    $data->delete();
    $message="One data deleted";
    return redirect('user.index',compact('message'));
    }else{
      return View('auth.login');
    }
  }
  public function addMapLink(Request $request){
    $user = \App\User::findOrFail($request->get('user_id'));
    $user->googleMapsLink = $request->get('googleMapsLink');
    $user->save();
    $url = "/users/".$request->get('user_id');
    return redirect::to($url);
  }

  public function deleteGoogleMapsLink(Request $request){
    $user = \App\User::findOrFail($request->get('user_id'));
    $user->googleMapsLink = null;
    $user->save();
    $url = "/users/".$request->get('user_id');
    return redirect::to($url);
  }
}
