@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All @if($message=='Favourites' || $message=='Flaged') {{$message}} @endif Products</h4>
                      <p class="category">Records regarding Product table</p>
                      @if($message != 'Flaged' && $message !='Favourites')
                      <div class="pull-right">
                          <a class="btn btn-primary" href="{{URL::asset('products/create')}}">Add New</a>
                      </div>
                      @endif
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>Product Primary Image</th>
                            <th>Product Title</th>
                            <th>Product Uploader</th>
                            <th>Product Status</th>
                            <th>Visits</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <?php
                                $profileImage=\App\Media::where('productId',$info->id)->where('mediaType', 'Image')->first();
                                ?>
                                @if(!empty($profileImage))
                                <td><img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto"/></td>
                                @else
                                <td><i class="ti-view-list-alt"></i></td>
                                @endif
                                <td>{{$info->productName}}</td>
                                <?php $userProfileId= \App\User::findOrFail($info->productOwner);
                                  //$userProfileImage= \App\Media::findOrFail($userProfileId->id);
                                ?>
                                <td><a href="/users/<?php echo $userProfileId->id;?>">{{$userProfileId->name}}</a></td>
                                <td>{{$info->productStatus}}</td>
                                <td>{{$info->productVisits}}</td>
                                <td>
                                  <a class="btn btn-warning" href="/products/<?php echo $info->id;?>/edit">Edit</a>
                                  <a href="/products/<?php echo $info->id; ?>" class="btn btn-success">View</a>
                                  <form action="{{ route('products.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                  </form>&nbsp
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>

@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
