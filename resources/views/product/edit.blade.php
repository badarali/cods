@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="content">
        <div class="col-md-12">
          <div class="card">
            <div class="header">
              <h4 class="title">Edit Product</h4>
            </div>
            <div class="content">
              <?php foreach ($media as $key => $value): ?>
              <div class="col-md-3">
                <?php if ($value->mediaType=='Image'): ?>
                  <img class="col-md-12" src="{{asset('product/images/').'/'.$value->mediaPath}}" alt="..."/>
                <?php else: ?>
                  <video class="col-md-12" controls>
  								  <source src="{{URL::asset('product/videos/').'/'.$value->mediaPath}}" type="video/mp4">
  								  Your browser does not support HTML5 video.
  								</video>
                <?php endif; ?>
                  <form action="{{ route('media.destroy', $value->id) }}" method="post" class="text-center">
                    <input type="text" hidden value="Product" name="type">
                    <input type="text" hidden value="{{$value->productId}}" name="otherid">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                  </form>
              </div>
              <?php endforeach; ?>
              <form action="/products/<?php echo $data->id;?>" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}
                {{method_field('PUT')}}
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Title</label>
                      <input value="{{$data->productName}}" type="text" class="form-control border-input" name="productName" placeholder="Enter Product Title" required/>
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Description</label>
                      <textarea class="form-control border-input" name="productDescriptionPath" cols="8" rows="8">{{$data->productDescriptionPath}}</textarea>
                    </div>
                  </div>


                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Product Unit Price (in Rupees)</label>
                          <input value="{{$data->productUnitPrice}}" type="number" class="form-control border-input" placeholder="Enter Product Unit Price" name="productUnitPrice" id="productUnitPrice" oninput="calculateDiscount()">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Product Unit Discount (in %)</label>
                          <input value="{{$data->productUnitDiscount}}" type="number" name="productUnitDiscount" class="form-control border-input" placeholder="Product Unit Discount" value="productUnitDiscount" id="productUnitDiscount" oninput="calculateDiscount()" max="100">
                      </div>
                  </div>


                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Product Total Price</label>
                          <input readonly value="{{$data->productTotalPrice}}" type="number" name="productTotalPrice" class="form-control border-input" placeholder="Product Total Price" id="productTotalPrice">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Product Status</label>
                          <select name="productStatus" class="form-control border-input">
                            @if($data->productStatus=='Active')
                            <option value="Active" selected>Active</option>
                            <option value="Deactive">Deactive</option>
                            @else
                            <option value="Active">Active</option>
                            <option value="Deactive" selected>Deactive</option>
                            @endif
                          </select>
                      </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Upload Images and Videos</label>
                      <input accept=".jpg,.jpeg,.png,.mkv,.mp4" type="file" name="files[]" class="form-control border-input" multiple="true"/>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <label>Category</label>
                        <?php $categories=\App\Category::all();?>
                        <select class="form-control border-input" name="categoryIds[]" multiple>

                          @<?php foreach ($categories as $key => $value): ?>
                            <?php $selected = false; ?>
                            <?php foreach ($selectedCategories as $key => $value1): ?>
                              @if($value1->categoryId==$value->id)
                              <?php $selected = true; ?>
                              <option value="{{$value->id}}" selected>{{$value->categoryName}}</option>
                              @endif
                            <?php endforeach; ?>
                            @if(!$selected)
                            <option value="{{$value->id}}">{{$value->categoryName}}</option>
                            @endif
                          <?php endforeach; ?>
                        </select>
                    </div>
                  </div>
                <div class="text-right col-md-12">
                  <button type="submit" class="btn btn-info btn-fill btn-wd">Update Product</button>
                </div>
                <div class="clearfix"></div>
                <input type="hidden" class="form-control border-input" name="productOwner" value="{{Auth::User()->id}}">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
    function calculateDiscount(){
      var unitPrice = document.getElementById('productUnitPrice').value;
      var unitDiscount = document.getElementById('productUnitDiscount').value;
      var discountedPrice = unitPrice -  (unitPrice * (unitDiscount/100));
      document.getElementById('productTotalPrice').value = discountedPrice;
    }
  </script>
</div>
@endsection
