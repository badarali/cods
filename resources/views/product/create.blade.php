@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Product</h4>
                    </div>
                    <div class="content">
                        <form action="/products" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Product Title</label>
                                        <input type="text" class="form-control border-input" name="productName" placeholder="Enter Product Title" required/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Product Description</label>
                                      <textarea placeholder="Enter Product Description" class="form-control border-input" name="productDescriptionPath" cols="8" rows="8"></textarea>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Product Unit Price (in Rupees)</label>
                                        <input type="number" class="form-control border-input" placeholder="Enter Product Unit Price" name="productUnitPrice" id="productUnitPrice" oninput="calculateDiscount()">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Product Unit Discount (in %)</label>
                                        <input type="number" name="productUnitDiscount" class="form-control border-input" placeholder="Product Unit Discount" value="0" id="productUnitDiscount" oninput="calculateDiscount()" max="100">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Product Discounted Price</label>
                                        <input type="number" name="productTotalPrice" class="form-control border-input" placeholder="Product Total Price" value="productTotalPrice" id="productTotalPrice" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Product Status</label>
                                        <select name="productStatus" class="form-control border-input">
                                          <option value="Active">Active</option>
                                          <option value="Deactive">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Upload Images and Videos</label>
                                        <input accept=".jpg,.jpeg,.png,.mkv,.mp4" type="file" name="files[]" class="form-control border-input" multiple="true"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Category</label>
                                      <?php $categories=\App\Category::all();?>
                                      <select class="form-control border-input" name="categoryIds[]" multiple>
                                        @<?php foreach ($categories as $key => $value): ?>
                                          <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                        <?php endforeach; ?>
                                      </select>
                                  </div>
                                </div>
                                <input type="hidden" class="form-control border-input" name="productOwner" value="{{Auth::User()->id}}">

                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">Add Product</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <script>
      function calculateDiscount(){
        var unitPrice = document.getElementById('productUnitPrice').value;
        var unitDiscount = document.getElementById('productUnitDiscount').value;
        var discountedPrice = unitPrice -  (unitPrice * (unitDiscount/100));
        document.getElementById('productTotalPrice').value = discountedPrice;
      }
    </script>
</div>
@endsection
@section('icon')
'ti-view-list-alt'
@endsection
@section('message')
"{{$message}}"
@endsection
@section('barcolor')
'success'
@endsection
