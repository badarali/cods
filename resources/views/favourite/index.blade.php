@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Categories</h4>
                      <p class="category">Records regarding Favourite table</p>
                      <div class="pull-right">
                          <a class="btn btn-primary" href="{{URL::asset('category/create')}}">Add New</a>
                      </div>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>ID</th>
                            <th>Favourite By User</th>
                            @if($info->toProfile)
                            <th>Favourite To User Profile</th>
                            @endif
                            <th>Favourite To Product</th>
                            <th>Favourite To Deal</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <td>{{$info->id}}</td>
                                @if($info->toProfile)
                                <td>{{App\User::where('id','$info->byUserId')->name;}}</td>
                                @endif
                                <td>{{$info->toProductId}}</td>
                                <td>{{$info->toDealId}}</td>

                                <td>
                                  <form action="{{ route('favourite.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                  <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                </form>&nbsp
                              </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>

                  </div>
              </div>
          </div>

        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
