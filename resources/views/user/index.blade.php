@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
@if(Auth::User()->role=='Super')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Users</h4>
                      <p class="category">Records regarding user table</p>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Account Status</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <?php $profileImage=\App\Media::select('mediaPath')->where('userProfileId',$info->id);?>
                                @if(empty($profileImage))
                                <td><img src="{{URL::asset('/uploads/profileImages/$profileImage')}}" width="75px" height="auto"/></td>
                                @else
                                <td><i class="ti-user"></i></td>
                                @endif
                                <td>{{$info->name}}</td>
                                <td>{{$info->email}}</td>
                                <td>{{$info->role}}</td>
                                <td>{{$info->profileStatus}}</td>
                                <td><a href="/users/<?php echo $info->id;?>">View</a>, <a href="/users/<?php echo $info->id;?>">Edit</a>&nbsp</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>

        </div>
    </div>
</div>
@endif
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
