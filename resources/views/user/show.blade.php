@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{URL::asset('assets/img/logo.png')}}" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                          <?php $profileImage=\App\Media::select('mediaPath')->where('userProfileId',$data->id)->orderBy('created_at','desc')->first();?>
                          @if(empty($profileImage))
                          <img class="avatar border-white" src="{{URL::asset('assets/img/faces/face-2.jpg')}}" alt="..."/>
                          @else
                          <img class="avatar border-white" src="{{URL::asset('profile/images/').'/'.$profileImage->mediaPath}}" alt="..."/>
                          @endif

                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                      <?php $id=$data->id;?>
                        <div class="row">
                          @if(Auth::User()->role == 'Super')
                            <a href="/user/<?php echo $data->id?>/statusNeedChange" style="margin:2%; padding:2%;">{{$data->profileStatus}}<a/>
                          @else
                          <h5>{{$data->profileStatus}}</h5>
                          @endif
                           </div>
                    </div>
                </div>
                @if(Auth::User()->role == 'Super' || Auth::User()->id == $data->id)
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Account</h4>
                        </div>
                        <div class="content">
                            <form action="{{ route('users.update', $data->id) }}" method="post"  enctype="multipart/form-data">
                              {{ method_field('PUT') }}
                              {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>User Name</label>
                                            <input type="text" class="form-control border-input" name="name" placeholder="{{$data->name}}" value="{{$data->name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">User Email</label>
                                            <input type="email" class="form-control border-input" name="email" placeholder="{{$data->email}}" value="{{$data->email}}" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Account Status</label>
                                            <select type="text" class="form-control border-input" name="profileStatus">
                                                <option value="Active">Active</option>
                                                <option value="Deactive">Deactive</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Upload Profile Image</label>
                                            <input type="file" class="form-control border-input" name="profileImage" accept=".jpg,.jpeg,.png" />
                                        </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Password <small>(Enter New If you want to change otherwise old)</small></label>
                                            <input type="password" class="form-control border-input" required name="password" placeholder="Enter New If you want to change otherwise old">
                                        </div>
                                    </div>
                                  </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                          <input type="submit" class="btn btn-info pull-right" value="Update"/>
                                        </div>
                                    </div>


                              </div>
                            </form>
                          </div>
                    </div>
                @endif
                <div class="card">
                    <div class="header">
                      @if(Auth::User()->role == 'Super' || Auth::User()->id == $data->id)
                        <h4 class="title">Registered Addresses of {{$data->name}}</h4>
                        <form action="/addresses" method="post" >
                            <div class="row">
                              <input type="hidden" value="{{$data->id}}" name="addressesOfUser"/>
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address Of</label>
                                        <select type="text" class="form-control border-input" name="addressesOf">
                                            <option value="Home">Home</option>
                                            <option value="Office">Office</option>
                                            <option value="Other">Other</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="text">Address</label>
                                        <input type="text" class="form-control border-input" name="addressesBody" placeholder="Add new address"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <input type="submit" class="btn btn-info pull-right" value="Add New"/>
                                    </div>
                                </div>
                                  <hr>
                            </div>


                          <div class="clearfix"></div>

                        </form>
                      @endif
                    </div>
                      <div class="content table-responsive table-full-width">
                          <table class="table table-striped">
                              <thead>
                                <th>Address Of</th>
                                <th>Address</th>
                                <th>Actions</th>
                              </thead>
                              <tbody>
                                <?php $userAddresses= \App\Address::where('addressesOfUser','=',$data->id)->get();?>
                                 @foreach($userAddresses as $address)
                                    <tr>
                                    <td>{{$address->addressesOf}}</td>
                                    <td>{{$address->addressesBody}}</td>
                                    @if(Auth::User()->role=='Super' || Auth::User()->id == $data->id)
                                    <td>
                                      <form action="{{ route('addresses.destroy', $address->id) }}" method="post">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{$data->id}}" name="ofUser"/>
                                        <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                      </form>
                                    </td>
                                    @endif
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>

                      </div>
                </div>

                <div class="card">
                    <div class="header">
                      @if(Auth::User()->role == 'Super' || Auth::User()->id == $data->id)
                        <h4 class="title">Registered Contacts of {{$data->name}}</h4>
                        <form action="/contacts" method="post" >
                            <div class="row">
                              <input type="hidden" value="{{$data->id}}" name="contactsOfUser"/>
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact Of</label>
                                        <select type="text" class="form-control border-input" name="contactsOf">
                                            <option value="Home">Home</option>
                                            <option value="Office">Office</option>
                                            <option value="Other">Other</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="text">Contact</label>
                                        <input type="text" class="form-control border-input" name="contactsBody" placeholder="Add new address"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <input type="submit" class="btn btn-info pull-right" value="Add New"/>
                                    </div>
                                </div>
                                  <hr>
                            </div>


                          <div class="clearfix"></div>

                        </form>
                        @endif
                    </div>
                      <div class="content table-responsive table-full-width">
                          <table class="table table-striped">
                              <thead>
                                <th>Contact Of</th>
                                <th>Contact</th>
                                <th>Actions</th>
                              </thead>
                              <tbody>
                                <?php $userAddresses= \App\Contact::where('contactsOfUser','=',$data->id)->get();?>
                                 @foreach($userAddresses as $address)
                                    <tr>
                                    <td>{{$address->contactsOf}}</td>
                                    <td>{{$address->contactsBody}}</td>
                                    @if(Auth::User()->role=='Super' || Auth::User()->id == $data->id)
                                    <td>
                                      <form action="{{ route('contacts.destroy', $address->id) }}" method="post">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{$data->id}}" name="ofUser"/>
                                        <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                      </form>
                                    </td>
                                    @endif
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                </div>
                <div class="card">
                  <div class="header">
                    @if(Auth::User()->role == 'Super' || Auth::User()->id == $data->id)
                    <h4 class="title">Location of {{$data->name}}</h4>
                    <form action="/addMapLink" method="post">
                      {{ csrf_field() }}
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Google Maps Link</label>
                            <input value="{{Auth::id()}}" name="user_id" hidden>
                            <input class="form-control border-input" type="link" placeholder="Google Maps Link" name="googleMapsLink">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group text-right">
                            <input class="btn btn-primary" type="submit" value="Add">
                          </div>
                        </div>
                    </div>
                    </form>
                    @endif
                  </div>
                  <div class="content table-responsive">
                    <table class="table table-striped table-hover">
                      <thead>
                        <th>Link</th>
                        <th>Actions</th>
                      </thead>
                      @if($data->googleMapsLink)
                      <tbody>
                        <td><a href="#" title="{{$data->googleMapsLink}}">Link</a></td>
                        <td>
                          <form action="/deleteGoogleMapsLink" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$data->id}}" name="user_id" />
                            <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                          </form>
                        </td>
                      </tbody>
                      @endif
                    </table>
                  </div>
                </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
