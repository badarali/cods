@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Product in Deal</h4>
                    </div>
                    <div class="content">
                        <form action="/productindeals" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Products</label>
                                        <?php $products=\App\Product::all();?>
                                        <select class="form-control border-input" name="productId">
                                          @<?php foreach ($products as $key => $value): ?>
                                            <option value="{{$value->id}}">{{$value->productName}}</option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Deals</label>
                                        <?php $deals=\App\Deal::all();?>
                                        <select class="form-control border-input" name="dealId">
                                          @<?php foreach ($deals as $key => $value): ?>
                                            <option value="{{$value->id}}">{{$value->dealName}}</option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control border-input" name="productindealsstatus">
                                          <option value="Active">Active</option>
                                          <option value="Deactive">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">Add Product in Deal</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
