@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
@if(Auth::User()->role=='Super')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Product in Deals</h4>
                      <p class="category">Records regarding Product in Deal table</p>
                      <div class="pull-right">
                          <a class="btn btn-primary" href="{{URL::asset('productindeals/create')}}">Add New</a>
                      </div>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>ID</th>
                            <th>Product Name</th>
                            <th>Deal Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <?php
                                $product=\App\Product::where('id',$info->productId)->first();
                                $deal=\App\Deal::where('id',$info->dealId)->first();
                              ?>
                              <tr>
                                <td>{{$info->id}}</td>
                                <td>{{$product->productName}}</td>
                                <td>{{$deal->dealName}}</td>
                                <td>{{$info->productindealsstatus}}</td>
                                <td>
                                  <a class="btn btn-warning" href="/productindeals/<?php echo $info->id;?>/edit">Edit</a>
                                  <form action="{{ route('productindeals.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                  <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                </form>&nbsp</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>

                  </div>
              </div>
          </div>

        </div>
    </div>
</div>
@endif
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
