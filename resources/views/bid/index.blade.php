@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Bids</h4>
                      <?php $message = "All Bids" ?>
                      <p class="category">Records regarding Bids table</p>

                      <div class="pull-right">
                          <a class="btn btn-primary" href="{{URL::asset('bid/create')}}">Add New</a>
                      </div>
                      <?php
                      if(Auth::User()->role=="Super"){
                        $data = \App\AssetBid::all();
                      }
                      else if(Auth::User()->role=="admin"){
                        $data = \App\AssetBid::where('sellerId', Auth::id())->get();
                      }
                      else if(Auth::User()->role=="client"){

                      }
                      else{

                      }
                      ?>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>Bid Id</th>
                            <th>Bid Asset</th>
                            <th>Bid Uploader</th>
                            <th>Bid Status</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <td>{{$info->id}}</td>
                                @if($info->dealId)
                                <?php $deal = \App\Deal::findOrFail($info->dealId); ?>
                                <td>{{$deal->dealName}}</td>
                                @elseif($info->productId)
                                <?php $product = \App\Product::findOrFail($info->productId); ?>
                                <td>{{$product->productName}}</td>
                                @endif
                                <?php $uploader = \App\User::findOrFail($info->sellerId); ?>
                                <td>{{$uploader->name}}</td>
                                <td>{{$info->status}}</td>
                                <td>
                                  <a href="/bid/<?php echo $info->id; ?>" class="btn btn-success">View</a>
                                  <form action="{{ route('bid.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                  </form>&nbsp
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
