@extends('layouts.app')
@section('title','Home Page')
@section('content')

<!-- /new_arrivals -->
  <div class="new_arrivals_agile_w3ls_info" id="app">
    <div class="container">
        <h3 class="wthree_text_info">Let's <span>hunt "Products"</span></h3>
        <div id="horizontalTab">
            <ul class="resp-tabs-list">
              <li> Latest </li>
              <li> Popular </li>
              <li> Trending </li>
              <li> Top Rated </li>
            </ul>
          <div class="resp-tabs-container">
            <div class="tab1">
              <?php
                $products = \App\Product::where('productStatus','Active')->orderBy('created_at', 'desc')->get();
              ?>
              <?php foreach ($products as $key => $value): ?>
                <div class="col-md-3 product-men">
                  <div class="men-pro-item simpleCart_shelfItem">
                    <div class="men-thumb-item">
                      <?php $profileImage=\App\Media::where('productId',$value->id)->first(); ?>
                      @if(!empty($profileImage))
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
                      @else
                      <i class="ti-view-list-alt"></i>
                      @endif
                        <div class="men-cart-pro">
                          <div class="inner-men-cart-pro">
                            <a href="/products/<?php echo $value->id; ?>" class="link-product-add-cart">Quick View</a>
                          </div>
                        </div>
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product ">
                      <h4><a href="/products/<?php echo $value->id; ?>">{{$value->productName}}</a></h4>
                      <?php
                      $rates = \App\Reviewandrating::where('toProductId', $value->id)->get();
                      if (!$rates->isEmpty()) {
                        $count = \App\Reviewandrating::where('toProductId', $value->id)->count();
                        $stars = 0;
                        foreach ($rates as $key => $value1) {
                          $stars = $stars + $value1->stars;
                        }
                        $avg = $stars / $count;
                        $fullStars = $avg*10;
                        $halfStar = 0;
                        if ($fullStars%10 > 0) {
                         $halfStar = 1;
                        }
                        $fullStars = floor($avg);
                        $starCount = $fullStars + $halfStar;
                        $remain = 5 - $starCount;
                        for ($i=0; $i < $fullStars; $i++) {
                          echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $halfStar; $i++) {
                          echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $remain; $i++) {
                          echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
                        }
                        echo " <small>($count)</small>";
                      }
                      ?>
                      <div class="info-product-price">
                        <span class="item_price">Rs {{$value->productTotalPrice}}</span>
                        <del>Rs {{$value->productUnitPrice}}</del>
                        <?php
                        $discount = $value->productUnitPrice-$value->productTotalPrice;
                        ?>
                      </div>
                      <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                        <form method="post">
                          <fieldset>
                            <input type="hidden" name="cmd" value="_cart" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="business" value="{{$value->id}}" />
                            <input type="hidden" name="item_name" value="{{$value->productName}}" />
                            <input type="hidden" name="amount" value="{{$value->productTotalPrice}}" />
                            <input type="hidden" name="discount_amount" value="{{$value->id}}" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="return" value=" " />
                            <input type="hidden" name="cancel_return" value=" " />
                            <input type="button" @click="add($event, '{{$value->id}}', 'Product', {{$value->productTotalPrice}}, '{{$value->productName}}')" value="Add to cart" class="button" />
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
              <div class="clearfix"></div>
            </div>
            <div class="tab2">
              <?php

              $orderMetas = \App\Ordermeta::all();
              $productids = array();
              $productOrderCounts = array();
              $productsByOrders = array();
              foreach ($orderMetas as $key => $value) {
                if (!empty($value->productId)) {
                  if (!in_array($value->productId, $productids)) {
                    $productids[] = $value->productId;
                    $productOrderCounts[] = $value->quantity;
                  }
                  else{
                    $index = array_search($value->productId, $productids);
                    $productOrderCounts[$index] = $productOrderCounts[$index] + $value->quantity;
                  }
                }
              }
              if (sizeof($productids)!=0) {
                dd(sizeof($productids));
                $productsWithCounts = array_combine($productids, $productOrderCounts);
                arsort($productsWithCounts);
                foreach ($productsWithCounts as $key => $value) {
                  $productsByOrders[] = \App\Product::findOrFail($key);
                }
              }

              ?>
              <?php foreach ($productsByOrders as $key => $value): ?>
                <div class="col-md-3 product-men">
                  <div class="men-pro-item simpleCart_shelfItem">
                    <div class="men-thumb-item">
                      <?php $profileImage=\App\Media::where('productId',$value->id)->first(); ?>
                      @if(!empty($profileImage))
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
                      @else
                      <i class="ti-view-list-alt"></i>
                      @endif
                        <div class="men-cart-pro">
                          <div class="inner-men-cart-pro">
                            <a href="/products/<?php echo $value->id; ?>" class="link-product-add-cart">Quick View</a>
                          </div>
                        </div>
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product ">
                      <h4><a href="/products/<?php echo $value->id; ?>">{{$value->productName}}</a></h4>
                      <?php
                      $rates = \App\Reviewandrating::where('toProductId', $value->id)->get();
                      if (!$rates->isEmpty()) {
                        $count = \App\Reviewandrating::where('toProductId', $value->id)->count();
                        $stars = 0;
                        foreach ($rates as $key => $value1) {
                          $stars = $stars + $value1->stars;
                        }
                        $avg = $stars / $count;
                        $fullStars = $avg*10;
                        $halfStar = 0;
                        if ($fullStars%10 > 0) {
                         $halfStar = 1;
                        }
                        $fullStars = floor($avg);
                        $starCount = $fullStars + $halfStar;
                        $remain = 5 - $starCount;
                        for ($i=0; $i < $fullStars; $i++) {
                          echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $halfStar; $i++) {
                          echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $remain; $i++) {
                          echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
                        }
                        echo " <small>($count)</small>";
                      }
                      ?>
                      <div class="info-product-price">
                        <span class="item_price">Rs {{$value->productTotalPrice}}</span>
                        <del>Rs {{$value->productUnitPrice}}</del>
                      </div>
                      <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                        <form method="post">
                          <fieldset>
                            <input type="hidden" name="cmd" value="_cart" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="business" value=" " />
                            <input type="hidden" name="item_name" value="Formal Blue Shirt" />
                            <input type="hidden" name="amount" value="30.99" />
                            <input type="hidden" name="discount_amount" value="1.00" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="return" value=" " />
                            <input type="hidden" name="cancel_return" value=" " />
                            <input type="button" @click="add($event, '{{$value->id}}', 'Product', {{$value->productTotalPrice}}, '{{$value->productName}}')" value="Add to cart" class="button" />
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
              <div class="clearfix"></div>
            </div>
            <div class="tab3">
              <?php
                $products = \App\Product::where('productStatus','Active')->orderBy('productVisits', 'desc')->get();
              ?>
              <?php foreach ($products as $key => $value): ?>
                <div class="col-md-3 product-men">
                  <div class="men-pro-item simpleCart_shelfItem">
                    <div class="men-thumb-item">
                      <?php $profileImage=\App\Media::where('productId',$value->id)->first(); ?>
                      @if(!empty($profileImage))
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
                      @else
                      <i class="ti-view-list-alt"></i>
                      @endif
                        <div class="men-cart-pro">
                          <div class="inner-men-cart-pro">
                            <a href="/products/<?php echo $value->id; ?>" class="link-product-add-cart">Quick View</a>
                          </div>
                        </div>
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product ">
                      <h4><a href="/products/<?php echo $value->id; ?>">{{$value->productName}}</a></h4>
                      <?php
                      $rates = \App\Reviewandrating::where('toProductId', $value->id)->get();
                      if (!$rates->isEmpty()) {
                        $count = \App\Reviewandrating::where('toProductId', $value->id)->count();
                        $stars = 0;
                        foreach ($rates as $key => $value1) {
                          $stars = $stars + $value1->stars;
                        }
                        $avg = $stars / $count;
                        $fullStars = $avg*10;
                        $halfStar = 0;
                        if ($fullStars%10 > 0) {
                         $halfStar = 1;
                        }
                        $fullStars = floor($avg);
                        $starCount = $fullStars + $halfStar;
                        $remain = 5 - $starCount;
                        for ($i=0; $i < $fullStars; $i++) {
                          echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $halfStar; $i++) {
                          echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $remain; $i++) {
                          echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
                        }
                        echo " <small>($count)</small>";
                      }
                      ?>
                      <div class="info-product-price">
                        <span class="item_price">Rs {{$value->productTotalPrice}}</span>
                        <del>Rs {{$value->productUnitPrice}}</del>
                      </div>
                      <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                        <form method="post">
                          <fieldset>
                            <input type="hidden" name="cmd" value="_cart" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="business" value=" " />
                            <input type="hidden" name="item_name" value="Formal Blue Shirt" />
                            <input type="hidden" name="amount" value="30.99" />
                            <input type="hidden" name="discount_amount" value="1.00" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="return" value=" " />
                            <input type="hidden" name="cancel_return" value=" " />
                            <input type="button" @click="add($event, '{{$value->id}}', 'Product', {{$value->productTotalPrice}}, '{{$value->productName}}')" value="Add to cart" class="button" />
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
              <div class="clearfix"></div>
            </div>
            <div class="tab4">
              <?php
              $reviews = \App\Reviewandrating::all();
              $productids = array();
              $productRates = array();
              $productsByRates = array();
              $counts = array();
              foreach ($reviews as $key => $value) {
                if (!empty($value->toProductId)) {
                  if (!in_array($value->toProductId, $productids)) {
                    $productids[] = $value->toProductId;
                    $productRates[] = $value->stars;
                    $counts[] = 1;
                  }
                  else{
                    $index = array_search($value->toProductId, $productids);
                    $productRates[$index] = $productRates[$index] + $value->stars;
                    $counts[$index] = $counts[$index] + 1;
                  }
                }
              }
              if (sizeof($productids)!=0) {
                foreach ($productids as $key => $value) {
                  $productRates[$key] = $productRates[$key] / $counts[$key];
                }
                $productsWithRates = array_combine($productids, $productRates);
                arsort($productsWithRates);
                foreach ($productsWithRates as $key => $value) {
                  $productsByRates[] = \App\Product::findOrFail($key);
                }
              }
              ?>
              <?php foreach ($productsByRates as $key => $value): ?>
                <div class="col-md-3 product-men">
                  <div class="men-pro-item simpleCart_shelfItem">
                    <div class="men-thumb-item">
                      <?php $profileImage=\App\Media::where('productId',$value->id)->first(); ?>
                      @if(!empty($profileImage))
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
                      <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
                      @else
                      <i class="ti-view-list-alt"></i>
                      @endif
                        <div class="men-cart-pro">
                          <div class="inner-men-cart-pro">
                            <a href="/products/<?php echo $value->id; ?>" class="link-product-add-cart">Quick View</a>
                          </div>
                        </div>
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product ">
                      <h4><a href="/products/<?php echo $value->id; ?>">{{$value->productName}}</a></h4>
                      <?php
                      $rates = \App\Reviewandrating::where('toProductId', $value->id)->get();
                      if (!$rates->isEmpty()) {
                        $count = \App\Reviewandrating::where('toProductId', $value->id)->count();
                        $stars = 0;
                        foreach ($rates as $key => $value1) {
                          $stars = $stars + $value1->stars;
                        }
                        $avg = $stars / $count;
                        $fullStars = $avg*10;
                        $halfStar = 0;
                        if ($fullStars%10 > 0) {
                         $halfStar = 1;
                        }
                        $fullStars = floor($avg);
                        $starCount = $fullStars + $halfStar;
                        $remain = 5 - $starCount;
                        for ($i=0; $i < $fullStars; $i++) {
                          echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $halfStar; $i++) {
                          echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
                        }
                        for ($i=0; $i < $remain; $i++) {
                          echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
                        }
                        echo " <small>($count)</small>";
                      }
                      ?>
                      <div class="info-product-price">
                        <span class="item_price">Rs {{$value->productTotalPrice}}</span>
                        <del>Rs {{$value->productUnitPrice}}</del>
                      </div>
                      <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                        <form method="post">
                          <fieldset>
                            <input type="hidden" name="cmd" value="_cart" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="business" value=" " />
                            <input type="hidden" name="item_name" value="Formal Blue Shirt" />
                            <input type="hidden" name="amount" value="30.99" />
                            <input type="hidden" name="discount_amount" value="1.00" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="return" value=" " />
                            <input type="hidden" name="cancel_return" value=" " />
                            <input type="button" @click="add($event, '{{$value->id}}', 'Product', {{$value->productTotalPrice}}, '{{$value->productName}}')" value="Add to cart" class="button" />
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- //new_arrivals -->
@endsection
