@extends('layouts.app')
@section('title','Home Page')
@section('content')

<!-- /new_arrivals -->
  <div class="new_arrivals_agile_w3ls_info" id="app">
    <div class="container">
        <h3 class="wthree_text_info">Let's <span>hunt "Deals"</span></h3>
        <?php
        $childCategories = \App\Subcategory::where('parentCategory',$id)->get();
        $subcategories = array();
        if (!$childCategories->isEmpty()) {
          echo "<div class='showProductCategories'>Sub Categories:&nbsp;";
          echo "<ul style='list-style:none;display:inline;'>";
        }
        foreach ($childCategories as $key => $value) {
          $subcategory = \App\Category::findOrFail($value->childCategory);
          echo "<li style='display:inline; background-color:#348e37; padding:1%; margin-right:1%;'><small><a href='/deal/$subcategory->categoryName/$subcategory->id' style='color:white;'>$subcategory->categoryName</a></small></li>";
        }
        if (!$childCategories->isEmpty()) {
          echo "</ul></div>";
        }
        ?>
        <?php foreach ($deals as $key => $value): ?>
          <div class="col-md-3 product-men">
            <div class="men-pro-item simpleCart_shelfItem">
              <div class="men-thumb-item">
                <?php $dealImage=\App\Media::where('dealId',$value->id)->first(); ?>
                @if(!empty($dealImage))
                <img src="{{asset('deal/images/').'/'.$dealImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
                <img src="{{asset('deal/images/').'/'.$dealImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
                @else
                <i class="ti-view-list-alt"></i>
                @endif
                  <div class="men-cart-pro">
                    <div class="inner-men-cart-pro">
                      <a href="/deals/<?php echo $value->id; ?>" class="link-product-add-cart">Quick View</a>
                    </div>
                  </div>
                  <span class="product-new-top">New</span>
              </div>
              <div class="item-info-product ">
                <h4><a href="/deals/<?php echo $value->id; ?>">{{$value->dealName}}</a></h4>
                <?php
                $rates = \App\Reviewandrating::where('toDealId', $value->id)->get();
                if (!$rates->isEmpty()) {
                  $count = \App\Reviewandrating::where('toDealId', $value->id)->count();
                  $stars = 0;
                  foreach ($rates as $key => $value1) {
                    $stars = $stars + $value1->stars;
                  }
                  $avg = $stars / $count;
                  $fullStars = $avg*10;
                  $halfStar = 0;
                  if ($fullStars%10 > 0) {
                   $halfStar = 1;
                  }
                  $fullStars = floor($avg);
                  $starCount = $fullStars + $halfStar;
                  $remain = 5 - $starCount;
                  for ($i=0; $i < $fullStars; $i++) {
                    echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
                  }

                  for ($i=0; $i < $halfStar; $i++) {
                    echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
                  }
                  for ($i=0; $i < $remain; $i++) {
                    echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
                  }
                  echo " <small>($count)</small>";
                }
                ?>
                <div class="info-product-price">
                  <span class="item_price">Rs {{$value->dealTotalPrice}}</span>
                  <del>Rs {{$value->dealUnitPrice}}</del>
                </div>
                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                  <form method="post">
                    <fieldset>
                      <input type="hidden" name="cmd" value="_cart" />
                      <input type="hidden" name="add" value="1" />
                      <input type="hidden" name="business" value=" " />
                      <input type="hidden" name="item_name" value="Formal Blue Shirt" />
                      <input type="hidden" name="amount" value="30.99" />
                      <input type="hidden" name="discount_amount" value="1.00" />
                      <input type="hidden" name="currency_code" value="USD" />
                      <input type="hidden" name="return" value=" " />
                      <input type="hidden" name="cancel_return" value=" " />
                      <input type="button" @click="add($event, '{{$value->id}}', 'Deal', {{$value->dealTotalPrice}}, '{{$value->dealName}}')" value="Add to cart" class="button" />
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  <!-- //new_arrivals -->
@endsection
