@extends('layouts.app')
@section('title','Single')
@section('content')
  <!-- banner-bootom-w3-agileits -->
<div class="banner-bootom-w3-agileits" id="app">
	<div class="container">
		<div class="col-md-12 text-center showProductCategories">
			<?php
			$assetcategories = \App\AssetCategory::where('dealId', $data->id)->get();
			$categoryids = array();
			foreach ($assetcategories as $key => $value) {
				if (!in_array($value->categoryId, $categoryids)) {
					$categories[] = \App\Category::where('id', $value->categoryId)->first();
					$categoryids[] = $value->categoryId;
				}
			}
			?>
			@if(!empty($categories))
			<div>
			<?php foreach ($categories as $key => $value): ?>
				 <small><a href="/deal/<?php echo "$value->categoryName/"; echo "$value->id"; ?>" style="display:inline; background-color:#348e37; padding:1%; margin-right:1%; color:white">{{$value->categoryName}}</a></small><?php if (sizeof($categories)-1>$key): ?>
				<?php endif; ?>
			<?php endforeach; ?>
			</div>
			@endif
		</div>
	   <div class="col-md-4 single-right-left ">
			<div class="grid images_3_of_2">
				<div class="flexslider">
					<ul class="slides">
            <?php
						$dealImages=\App\Media::where('dealId',$data->id)->where('mediaType', 'Image')->get();
						$dealVideos=\App\Media::where('dealId',$data->id)->where('mediaType', 'Video')->get();
						?>
            <?php foreach ($dealImages as $key => $value): ?>
              <li data-thumb="{{URL::asset('deal/images/').'/'.$value->mediaPath}}">
  							<div class="thumb-image"> <img src="{{URL::asset('deal/images/').'/'.$value->mediaPath}}" data-imagezoom="true" class="img-responsive"> </div>
  						</li>
            <?php endforeach; ?>
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-8 single-right-left simpleCart_shelfItem">
			<?php
			$data->dealVisits = $data->dealVisits + 1;
			$data->save();
			?>
					<h3>{{$data->dealName}}
						<?php
						$checkFavourite = \App\Favourite::where('byUserId', Auth::id())->where('toDealId', $data->id)->first();
						?>
						@if(!empty($checkFavourite))
						<form action="{{ route('favourite.destroy', $checkFavourite->id) }}" method="post" style="display:inline">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<input type="text" value="{{$data->id}}" name="dealId" hidden>
							<input type="text" value="Deal" name="type" hidden>
							<button style="border:none; background-color:white" class="favLinkChecked" type="submit"><i class="fas fa-heart" title="Remove From Favourite"></i></button>
						</form>
						@else
						<form action="/favourite" method="post" style="display:inline">
              {{ csrf_field() }}
              <input type="text" value="{{Auth::id()}}" name="byUserId" hidden>
              <input type="text" value="{{$data->id}}" name="toDealId" hidden>
              <input type="text" value="Deal" name="type" hidden>
              <button style="border:none; background-color:white" class="favLink" type="sumbit"><i class="fas fa-heart" title="Favourite"></i></button>
            </form>
						@endif

						<?php
						$checkFlag = \App\Flag::where('byUserId', Auth::id())->where('toDealId', $data->id)->first();
						?>
						@if(!empty($checkFlag))
						<form action="{{ route('flag.destroy', $checkFlag->id) }}" method="post" style="display:inline">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<input type="text" value="{{$data->id}}" name="dealId" hidden>
							<input type="text" value="Deal" name="type" hidden>
							<button style="border:none; background-color:white" class="flagLinkChecked" type="submit"><i class="fas fa-flag" title="Remove From Flag"></i></button>
						</form>
						@else
						<form action="/flag" method="post" style="display:inline">
              {{ csrf_field() }}
              <input type="text" value="{{Auth::id()}}" name="byUserId" hidden>
              <input type="text" value="{{$data->id}}" name="toDealId" hidden>
              <input type="text" value="Deal" name="type" hidden>
              <button style="border:none ; background-color:white" class="flagLink" type="submit"><i class="fas fa-flag" title="Flag"></i></button>
            </form>
						@endif
						<div class="occasion-cart snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2 pull-right">
                            <form method="post">
                              <fieldset>
                                <input type="hidden" name="cmd" value="_cart">
                                <input type="hidden" name="add" value="1">
                                <input type="hidden" name="business" value=" ">
                                <input type="hidden" name="item_name" value="Wing Sneakers">
                                <input type="hidden" name="amount" value="650.00">
                                <input type="hidden" name="discount_amount" value="1.00">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="return" value=" ">
                                <input type="hidden" name="cancel_return" value=" ">
                                <input type="button" @click="add($event, '{{$data->id}}', 'Deal', {{$data->dealTotalPrice}}, '{{$data->dealName}}')" value="Add to cart" class="button">
                              </fieldset>
                            </form>
              </div>
						</h3>
						<?php
						$rates = \App\Reviewandrating::where('toDealId', $data->id)->get();
						if (!$rates->isEmpty()) {
							$count = \App\Reviewandrating::where('toDealId', $data->id)->count();
							$stars = 0;
							foreach ($rates as $key => $value) {
								$stars = $stars + $value->stars;
							}
							$avg = $stars / $count;
							$fullStars = $avg*10;
							$halfStar = 0;
							if ($fullStars%10 > 0) {
							 $halfStar = 1;
							}
							$fullStars = floor($avg);
							$starCount = $fullStars + $halfStar;
							$remain = 5 - $starCount;
							for ($i=0; $i < $fullStars; $i++) {
								echo '<i class="fas fa-star starChecked"></i>';
							}

							for ($i=0; $i < $halfStar; $i++) {
								echo '<i class="fas fa-star-half-alt starChecked"></i>';
							}
							for ($i=0; $i < $remain; $i++) {
								echo '<i class="fas fa-star starUnchecked"></i>';
							}
							echo " <small>($count Reviews)</small>";
						}
						?>
						<p><span class="item_price">Rs {{$data->dealTotalPrice}}</span> <del>- Rs {{$data->dealUnitPrice}}</del></p>

					<div class="description">
						{{$data->dealDescriptionPath}}
						<?php foreach ($dealVideos as $key => $value): ?>
							<br><br>
							<video class="col-md-12" controls>
							  <source src="{{URL::asset('deal/videos/').'/'.$value->mediaPath}}" type="video/mp4">
							  Your browser does not support HTML5 video.
							</video>
						<?php endforeach; ?>
					</div>
					<div class="color-quality">
						<div class="color-quality-right">
							<h5>Quantity:</h5>
              <input type="number" name="quantity" class="frm-field required sect" placeholder="Quantity" value="1">
						</div>
					</div>
		      </div>
	 			<div class="clearfix"> </div>
				<div class="col-md-12">
					<?php
					$user = \App\User::findOrFail($data->dealOwner);
					$iframeLink = $user->googleMapsLink;
					?>
					<iframe src="{{$iframeLink}}" frameborder="0" style="border:0;" height="400em" class="col-md-12" allowfullscreen></iframe>
				</div>
				<!-- /new_arrivals -->
	<div class="responsive_tabs_agileits">
				<div id="horizontalTab">
						<ul class="resp-tabs-list">
							<li>Reviews</li>
						</ul>
					<div class="resp-tabs-container">
						<div class="tab2">

							<div class="single_page_agile_its_w3ls">
								<div class="bootstrap-tab-text-grids">
									@if($message=='Rate')
									<div class="bootstrap-tab-text-grid">
										<div class="add-review">
	 										<h4>add a review</h4>
	 										<form action="/reviewandrating" method="POST" enctype="multipart/form-data">
												{{ csrf_field() }}
	 											<input type="text" value="{{$data->id}}" hidden name="toDealId">
	 											<div class="rating1">
	 												<fieldset class="starability-basic" style="min-height:auto;">
	 												  <input type="radio" id="no-rate" class="input-no-rate" name="rating" value="0" checked aria-label="No rating." />
	 												  <input type="radio" id="first-rate1" name="rating" value="1" />
	 												  <label for="first-rate1" title="Terrible">1 star</label>
	 												  <input type="radio" id="first-rate2" name="rating" value="2" />
	 												  <label for="first-rate2" title="Not good">2 stars</label>
	 												  <input type="radio" id="first-rate3" name="rating" value="3" />
	 												  <label for="first-rate3" title="Average">3 stars</label>
	 												  <input type="radio" id="first-rate4" name="rating" value="4" />
	 												  <label for="first-rate4" title="Very good">4 stars</label>
	 												  <input type="radio" id="first-rate5" name="rating" value="5" />
	 												  <label for="first-rate5" title="Amazing">5 stars</label>
	 												</fieldset>
	 											</div>
												<input type="text" value="Deal" name="type" hidden>
	 											<textarea name="review" placeholder="Review"></textarea>
	 											<input type="submit" value="SEND">
	 										</form>
	 									</div>
									</div>
									@endif
									<?php
									$reviews = \App\Reviewandrating::where('toDealId', $data->id)->get();
									?>
									<?php foreach ($reviews as $key => $value): ?>
									<?php $userImage=\App\Media::where('userProfileId',$value->byUserId)->first(); ?>
									<div class="bootstrap-tab-text-grid">
										<?php if ($userImage): ?>
											<div class="bootstrap-tab-text-grid-left">
												<img src="{{URL::asset('profile/images/').'/'.$userImage->mediaPath}}" alt=" " class="img-responsive">
											</div>
										<?php else: ?>
											<div class="bootstrap-tab-text-grid-left">
												<i class="fas fa-user"></i>
											</div>
										<?php endif; ?>
											<div class="bootstrap-tab-text-grid-right">
												<ul>
													<?php
													$user = \App\User::where('id', $value->byUserId)->first();
													?>
													<li>
														@if(Auth::User()->role=='Super')
														<a href="/users/<?php echo $user->id;?>">{{$user->name}}</a>
														@else
														{{$user->name}}
														@endif
													</li>
													<li>
														<?php
														for ($i=0; $i < $value->stars; $i++) {
															echo '<i class="fas fa-star starChecked"></i>';
														}
														$remain = 5 - $value->stars;
														for ($i=0; $i < $remain; $i++) {
															echo '<i class="fas fa-star starUnchecked"></i>';
														}
														?>
													</li>
												</ul>
												<p>{{$value->reviewPath}}</p>
											</div>

										<div class="clearfix"> </div>
						       </div>
									 <?php endforeach; ?>
								 </div>
							 </div>
						 </div>
					</div>
				</div>
			</div>
	<!-- //new_arrivals -->
	  	<!--/slider_owl-->
			<div class="w3_agile_latest_arrivals">
				<h3 class="wthree_text_info">Similar <span>Deals</span></h3>
		  </div>
			<?php foreach ($deals as $key => $value): ?>
				<div class="col-md-3 product-men">
					<div class="men-pro-item simpleCart_shelfItem">
						<div class="men-thumb-item">
							<?php $dealImage=\App\Media::where('dealId',$value->id)->first(); ?>
							@if(!empty($dealImage))
							<img src="{{asset('deal/images/').'/'.$dealImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
							<img src="{{asset('deal/images/').'/'.$dealImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
							@else
							<i class="ti-view-list-alt"></i>
							@endif
								<div class="men-cart-pro">
									<div class="inner-men-cart-pro">
										<a href="single.html" class="link-product-add-cart">Quick View</a>
									</div>
								</div>
								<span class="product-new-top">New</span>
						</div>
						<div class="item-info-product ">
							<h4><a href="/deals/<?php echo $value->id; ?>">{{$value->dealName}}</a></h4>
							<?php
							$rates = \App\Reviewandrating::where('toDealId', $value->id)->get();
							if (!$rates->isEmpty()) {
								$count = \App\Reviewandrating::where('toDealId', $value->id)->count();
								$stars = 0;
								foreach ($rates as $key => $value1) {
									$stars = $stars + $value1->stars;
								}
								$avg = $stars / $count;
								$fullStars = $avg*10;
								$halfStar = 0;
								if ($fullStars%10 > 0) {
								 $halfStar = 1;
								}
								$fullStars = floor($avg);
								$starCount = $fullStars + $halfStar;
								$remain = 5 - $starCount;
								for ($i=0; $i < $fullStars; $i++) {
									echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
								}

								for ($i=0; $i < $halfStar; $i++) {
									echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
								}
								for ($i=0; $i < $remain; $i++) {
									echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
								}
								echo "<br><small>($count Reviews)</small>";
							}
							?>
							<div class="info-product-price">
								<span class="item_price">Rs {{$value->dealTotalPrice}}</span>
								<del>Rs {{$value->dealUnitPrice}}</del>
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
								<form method="post">
									<fieldset>
										<input type="hidden" name="cmd" value="_cart" />
										<input type="hidden" name="add" value="1" />
										<input type="hidden" name="business" value=" " />
										<input type="hidden" name="item_name" value="Formal Blue Shirt" />
										<input type="hidden" name="amount" value="30.99" />
										<input type="hidden" name="discount_amount" value="1.00" />
										<input type="hidden" name="currency_code" value="USD" />
										<input type="hidden" name="return" value=" " />
										<input type="hidden" name="cancel_return" value=" " />
										<input type="button" @click="add($event, '{{$value->id}}', 'Deal', {{$value->dealTotalPrice}}, '{{$value->dealName}}')" value="Add to cart" class="button" />
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
	   </div>
 </div>
@endsection
