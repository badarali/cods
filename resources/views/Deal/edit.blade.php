@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Deal</h4>
                    </div>
                    <div class="content">
                      <?php foreach ($media as $key => $value): ?>
                      <div class="col-md-3 col-sm-12">
                        <?php if ($value->mediaType=='Image'): ?>
                          <img class="col-md-12" src="{{asset('deal/images/').'/'.$value->mediaPath}}" alt="..."/>
                        <?php else: ?>
                          <video class="col-md-12" controls>
                            <source src="{{URL::asset('deal/videos/').'/'.$value->mediaPath}}" type="video/mp4">
                            Your browser does not support HTML5 video.
                          </video>
                        <?php endif; ?>
                          <form action="{{ route('media.destroy', $value->id) }}" method="post" class="text-center">
                            <input type="text" hidden value="Deal" name="type">
                            <input type="text" hidden value="{{$value->dealId}}" name="otherid">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                          </form>
                      </div>
                      <?php endforeach; ?>
                        <form action="/deals/<?php echo $data->id;?>" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          {{method_field('PUT')}}
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Deal Name</label>
                                  <input type="text" class="form-control border-input" name="dealName" placeholder="Enter Deal Title" value="{{$data->dealName}}" required/>
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>Deal Description</label>
                                <textarea placeholder="Enter Deal Description" class="form-control border-input" name="dealDescriptionPath" cols="8" rows="8">{{$data->dealDescriptionPath}}</textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Unit Price (in Rupees)</label>
                                  <input type="number" class="form-control border-input" placeholder="Enter Deal Unit Price" name="dealUnitPrice" id="dealUnitPrice" oninput="calculateDiscount()" value="{{$data->dealUnitPrice}}">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Unit Discount (in %)</label>
                                  <input type="number" name="dealUnitDiscount" class="form-control border-input" placeholder="Deal Unit Discount" value="0" id="dealUnitDiscount" oninput="calculateDiscount()" max="100" value="{{$data->dealUnitDiscount}}">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Discounted Price</label>
                                  <input type="number" name="dealTotalPrice" class="form-control border-input" placeholder="Deal Total Price" id="dealTotalPrice" readonly value="{{$data->dealTotalPrice}}">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Status</label>
                                  <select name="dealStatus" class="form-control border-input">
                                    @if($data->dealStatus=='Active')
                                    <option value="Active" selected>Active</option>
                                    <option value="Deactive">Deactive</option>
                                    @else
                                    <option value="Active">Active</option>
                                    <option value="Deactive" selected>Deactive</option>
                                    @endif
                                  </select>
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Upload Images and Videos</label>
                                  <input type="file" name="files[]" class="form-control border-input" multiple="true" accept=".mp4,.jpg,.jpeg,.png,.mkv"/>
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>Category</label>
                                <?php $categories=\App\Category::all();?>
                                <select class="form-control border-input" name="categoryIds[]" multiple>

                                  @<?php foreach ($categories as $key => $value): ?>
                                    <?php $selected = false; ?>
                                    <?php foreach ($selectedCategories as $key => $value1): ?>
                                      @if($value1->categoryId==$value->id)
                                      <?php $selected = true; ?>
                                      <option value="{{$value->id}}" selected>{{$value->categoryName}}</option>
                                      @endif
                                    <?php endforeach; ?>
                                    @if(!$selected)
                                    <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                    @endif
                                  <?php endforeach; ?>
                                </select>
                            </div>
                          </div>
                          <input type="hidden" class="form-control border-input" name="dealOwner" value="{{Auth::User()->id}}">
                          <div class="pull-right">
                              <button type="submit" class="btn btn-success btn-fill btn-wd">Update Deal</button>
                          </div>
                          <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <script>
      function calculateDiscount(){
        var unitPrice = document.getElementById('dealUnitPrice').value;
        var unitDiscount = document.getElementById('dealUnitDiscount').value;
        var discountedPrice = unitPrice -  (unitPrice * (unitDiscount/100));
        document.getElementById('dealTotalPrice').value = discountedPrice;
      }
    </script>
</div>
@endsection
