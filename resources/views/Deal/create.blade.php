@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Deal</h4>
                    </div>
                    <div class="content">
                        <form action="/deals" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Deal Name</label>
                                  <input type="text" class="form-control border-input" name="dealName" placeholder="Enter Deal Title" required/>
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>Deal Description</label>
                                <textarea placeholder="Enter Deal Description" class="form-control border-input" name="dealDescriptionPath" cols="8" rows="8"></textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Unit Price (in Rupees)</label>
                                  <input type="number" class="form-control border-input" placeholder="Enter Deal Unit Price" name="dealUnitPrice" id="dealUnitPrice" oninput="calculateDiscount()">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Unit Discount (in %)</label>
                                  <input type="number" name="dealUnitDiscount" class="form-control border-input" placeholder="Deal Unit Discount" value="0" id="dealUnitDiscount" oninput="calculateDiscount()" max="100">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Discounted Price</label>
                                  <input type="number" name="dealTotalPrice" class="form-control border-input" placeholder="Deal Total Price" value="dealTotalPrice" id="dealTotalPrice" readonly>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deal Status</label>
                                  <select name="dealStatus" class="form-control border-input">
                                    <option value="Active">Active</option>
                                    <option value="Deactive">Deactive</option>
                                  </select>
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Upload Images and Videos</label>
                                  <input type="file" name="files[]" class="form-control border-input" multiple="true" accept=".mp4,.jpg,.jpeg,.png,.mkv" />
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>Category</label>
                                <?php $categories=\App\Category::all();?>
                                <select class="form-control border-input" name="categoryIds[]" multiple>
                                  @<?php foreach ($categories as $key => $value): ?>
                                    <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                  <?php endforeach; ?>
                                </select>
                            </div>
                          </div>
                          <input type="hidden" class="form-control border-input" name="dealOwner" value="{{Auth::User()->id}}">
                          <div class="pull-right">
                              <button type="submit" class="btn btn-success btn-fill btn-wd">Add Deal</button>
                          </div>
                          <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <script>
      function calculateDiscount(){
        var unitPrice = document.getElementById('dealUnitPrice').value;
        var unitDiscount = document.getElementById('dealUnitDiscount').value;
        var discountedPrice = unitPrice -  (unitPrice * (unitDiscount/100));
        document.getElementById('dealTotalPrice').value = discountedPrice;
      }
    </script>
</div>
@endsection
