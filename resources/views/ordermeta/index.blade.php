@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
@if(Auth::User()->role=='Super' || Auth::User()->role=='Admin' || Auth::User()->role=='Client')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Orders</h4>
                      <p class="category">Records regarding Order table</p>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>Order Id</th>
                            @if(Auth::User()->role=='Super')
                            <th>Order By</th>
                            @endif
                            <th>Order Status</th>
                            <th>Order Placed</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <td>{{$info->id}}</td>
                                <?php
                                $order = \App\Order::where('orderconnectId', $info->id)->first();
                                $user = \App\User::where('id', $order->orderByUserId)->first();
                                $message = "Showing All Orders";
                                ?>
                                @if(Auth::User()->role=='Super')
                                <td>
                                  <a href="/users/<?php echo $user->id; ?>">{{$user->name}}</a>
                                </td>
                                @endif
                                <td>{{$info->orderStatus}}</td>
                                <td>{{$info->created_at}}</td>
                                <td>
                                  @if(Auth::User()->role=='Super')
                                  <a class="btn btn-warning" href="/order/<?php echo $info->id;?>/edit">Edit</a>
                                  @endif
                                  <a href="/order/<?php echo $info->id; ?>" class="btn btn-success">View</a>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>

        </div>
    </div>
</div>
@endif
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
