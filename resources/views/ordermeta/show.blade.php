@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Order Id: {{$data->id}} <small>({{$data->orderStatus}})</small></h4>
                    </div>
                    <div class="content">
                      <?php
                      $order = \App\Order::where('orderconnectId', $data->id)->first();
                      $address = \App\Address::findOrFail($order->orderOnAddressId);
                      $contact = \App\Contact::findOrFail($order->orderOnContactId);
                      $message = "One Order Requested";
                      ?>
                      @if(!Auth::User()->role!='Admin')
                      <div class="col-md-12">
                        <b>Address: </b><p class="category" style="display:inline">{{$address->addressesBody}}</p><br>
                        <b>Contact: </b><p class="category" style="display:inline">{{$contact->contactsBody}}</p>
                      </div>
                      @endif
                      <div class="content table-responsive table-full-width">
                          <table class="table table-striped">
                              <thead>
                                <th>Asset Name</th>
                                @if(Auth::User()->role=='Super')
                                <th>Order By</th>
                                @endif
                                <th>Type</th>
                                <th>Quantity</th>
                                @if(Auth::User()->role=='Super')
                                <th>Seller</th>
                                @endif
                                <th>Price</th>
                              </thead>
                              @if(Auth::User()->role=='Super' || Auth::User()->role=='Client')
                              <tbody>
                                <?php
                                $assets = \App\Ordermeta::where('orderconnectId', $data->id)->get();
                                $total = 0;
                                ?>
                                @foreach($assets as $info)
                                <?php
                                if (!empty($info->productId)) {
                                  $asset = \App\Product::where('id', $info->productId)->first();
                                  $seller = $asset->productOwner;
                                  $seller = \App\User::where('id', $seller)->first();
                                  $type = "Product";
                                  $name = $asset->productName;
                                  $price = $asset->productTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                else{
                                  $asset = \App\Deal::where('id', $info->dealId)->first();
                                  $type = "Deal";
                                  $seller = $asset->dealOwner;
                                  $seller = \App\User::where('id', $seller)->first();
                                  $name = $asset->dealName;
                                  $price = $asset->dealTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                ?>
                                  <tr>
                                    <td>{{$name}}</td>
                                    <?php
                                    $user = \App\Order::where('orderconnectId', $data->id)->first();
                                    $user = \App\User::where('id', $user->orderByUserId)->first();
                                    ?>
                                    @if(Auth::User()->role=='Super')
                                    <td>
                                      <a href="/users/<?php echo $user->id; ?>">{{$user->name}}</a>
                                    </td>
                                    @endif
                                    <td>{{$type}}</td>
                                    <td>{{$info->quantity}}</td>
                                    @if(Auth::User()->role=='Super')
                                    <td><a href="/users/<?php echo $seller->id; ?>">{{$seller->name}}</a></td>
                                    @endif
                                    <td>{{$price}}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                              @endif
                              @if(Auth::User()->role=='Admin')
                              <tbody>
                                <?php
                                $order = \App\Order::where('orderconnectId', $data->id)->where('orderToSellerId', Auth::id())->first();
                                $assets = \App\Ordermeta::where('orderconnectId', $data->id)->where('orderId', $order->id)->get();
                                $total = 0;
                                ?>
                                @foreach($assets as $info)
                                <?php
                                if (!empty($info->productId)) {
                                  $asset = \App\Product::where('id', $info->productId)->first();
                                  $type = "Product";
                                  $seller = $asset->productOwner;
                                  $seller = \App\User::where('id', $seller)->first();
                                  $name = $asset->productName;
                                  $price = $asset->productTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                else{
                                  $asset = \App\Deal::where('id', $info->dealId)->first();
                                  $type = "Deal";
                                  $seller = $asset->dealOwner;
                                  $seller = \App\User::where('id', $seller)->first();
                                  $name = $asset->dealName;
                                  $price = $asset->dealTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                ?>
                                  <tr>
                                    <td>{{$name}}</td>
                                    <?php
                                    $user = \App\Order::where('orderconnectId', $data->id)->first();
                                    $user = \App\User::where('id', $user->orderByUserId)->first();
                                    ?>
                                    @if(Auth::User()->role=='Super')
                                    <td><a href="/users/<?php echo $user->id; ?>">{{$user->name}}</a></td>
                                    @endif
                                    <td>{{$type}}</td>
                                    <td>{{$info->quantity}}</td>
                                    @if(Auth::User()->role=='Super')
                                    <td><a href="/users/<?php echo $seller->id; ?>">{{$seller->name}}</a></td>
                                    @endif
                                    <td>{{$price}}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                              @endif
                          </table>
                      </div>
                      <div>
                        <h4 class="title">Total: <p class="category" style="display:inline">{{$total}}</p> </h4>
                      </div>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
