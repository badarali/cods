@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
@if(Auth::User()->role=='Super')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Order Id: {{$data->id}} <small>({{$data->orderStatus}})</small></h4>
                    </div>
                    <div class="content">
                      <form action="/order/<?php echo $data->id;?>" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Order Status</label>
                              <select class="form-control border-input" name="orderStatus">
                                <option value="Active">Active</option>
                                <option value="Process">Process</option>
                                <option value="Complete">Complete</option>
                                <option value="Closed">Closed</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6"></div>
                          <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Update</button>
                          </div>
                      </form>
                      <div class="content table-responsive table-full-width">
                          <table class="table table-striped">
                              <thead>
                                <th>Asset Name</th>
                                <th>Order By</th>
                                <th>Type</th>
                                <th>Quantity</th>
                                <th>Price</th>
                              </thead>
                              @if(Auth::User()->role=='Super' || Auth::User()->role=='Client')
                              <tbody>
                                <?php
                                $assets = \App\Ordermeta::where('orderconnectId', $data->id)->get();
                                $total = 0;
                                ?>
                                @foreach($assets as $info)
                                <?php
                                if (!empty($info->productId)) {
                                  $asset = \App\Product::where('id', $info->productId)->first();
                                  $type = "Product";
                                  $name = $asset->productName;
                                  $price = $asset->productTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                else{
                                  $asset = \App\Deal::where('id', $info->dealId)->first();
                                  $type = "Deal";
                                  $name = $asset->dealName;
                                  $price = $asset->dealTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                ?>
                                  <tr>
                                    <td>{{$name}}</td>
                                    <?php
                                    $user = \App\Order::where('orderconnectId', $data->id)->first();
                                    $user = \App\User::where('id', $user->orderByUserId)->first();
                                    ?>
                                    <td><a href="/users/<?php echo $user->id; ?>">{{$user->name}}</a></td>
                                    <td>{{$type}}</td>
                                    <td>{{$info->quantity}}</td>
                                    <td>{{$price}}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                              @endif
                              @if(Auth::User()->role=='Admin')
                              <tbody>
                                <?php
                                $order = \App\Order::where('orderconnectId', $data->id)->where('orderToSellerId', Auth::id())->first();
                                $assets = \App\Ordermeta::where('orderconnectId', $data->id)->where('orderId', $order->id)->get();
                                $total = 0;
                                ?>
                                @foreach($assets as $info)
                                <?php
                                if (!empty($info->productId)) {
                                  $asset = \App\Product::where('id', $info->productId)->first();
                                  $type = "Product";
                                  $name = $asset->productName;
                                  $price = $asset->productTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                else{
                                  $asset = \App\Deal::where('id', $info->dealId)->first();
                                  $type = "Deal";
                                  $name = $asset->dealName;
                                  $price = $asset->dealTotalPrice * $info->quantity;
                                  $total = $total + $price;
                                }
                                ?>
                                  <tr>
                                    <td>{{$name}}</td>
                                    <?php
                                    $user = \App\Order::where('orderconnectId', $data->id)->first();
                                    $user = \App\User::where('id', $user->orderByUserId)->first();
                                    ?>
                                    <td><a href="/users/<?php echo $user->id; ?>">{{$user->name}}</a></td>
                                    <td>{{$type}}</td>
                                    <td>{{$info->quantity}}</td>
                                    <td>{{$price}}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                              @endif
                          </table>
                      </div>
                      <div>
                        <h4 class="title">Total: <p class="category" style="display:inline">{{$total}}</p> </h4>
                      </div>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
@endif
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
