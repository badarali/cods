@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Bid</h4>
                    </div>
                    <div class="content">
                        <form action="/assetbid" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product</label>
                                  <?php $products=\App\Product::where('productStatus', 'Active')->get();?>
                                  <select class="form-control border-input" required name="productId">
                                    <option value="null">Select Product</option>
                                    @<?php foreach ($products as $key => $value): ?>
                                      <option value="{{$value->id}}">{{$value->productName}}</option>
                                    <?php endforeach; ?>
                                  </select>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Deals</label>
                                  <?php $deals=\App\Deal::where('dealStatus', 'Active')->get();?>
                                  <select class="form-control border-input" required name="dealId">
                                    <option value="null">Select Deal</option>
                                    @<?php foreach ($deals as $key => $value): ?>
                                      <option value="{{$value->id}}">{{$value->dealName}}</option>
                                    <?php endforeach; ?>
                                  </select>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Base Amount</label>
                                  <input type="number" class="form-control border-input" placeholder="Enter Base Amount" name="baseAmount" required min="1">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Time <small>(In Minutes)</small></label>
                                  <input type="number" name="time" class="form-control border-input" placeholder="Enter Time" required min="1">
                              </div>
                          </div>
                          <input type="hidden" class="form-control border-input" name="sellerId" value="{{Auth::User()->id}}">
                          <div class="col-md-12">
                              <button type="submit" class="btn btn-success btn-fill btn-wd pull-right">Add Bid</button>
                          </div>
                          <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
