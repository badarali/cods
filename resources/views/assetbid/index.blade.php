@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Assets On Bids</h4>
                      <?php $message = "All Bids" ?>
                      <p class="category">Records regarding Assets On Bids table</p>
                      <div class="pull-right">
                          <a class="btn btn-primary" href="{{URL::asset('assetbid/create')}}">Add New</a>
                      </div>
                      <?php
                      if(Auth::User()->role=="Super"){
                        $data = \App\AssetBid::all();
                      }
                      else if(Auth::User()->role=="Admin"){
                        $data = \App\AssetBid::where('sellerId', Auth::id())->get();
                      }
                      else if(Auth::User()->role=="Client"){
                        $data = \App\Bid::where('byUserId', Auth::id())->get();
                      }
                      else{

                      }
                      ?>
                  </div>
                  @if(Auth::User()->role=='Super' || Auth::User()->role=='Admin')
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>Id</th>
                            <th>Asset On Bid</th>
                            <th>Uploader</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <td>{{$info->id}}</td>
                                @if($info->dealId)
                                <?php $deal = \App\Deal::findOrFail($info->dealId); ?>
                                <td>{{$deal->dealName}}</td>
                                @elseif($info->productId)
                                <?php $product = \App\Product::findOrFail($info->productId); ?>
                                <td>{{$product->productName}}</td>
                                @endif
                                <?php $uploader = \App\User::findOrFail($info->sellerId); ?>
                                <td>{{$uploader->name}}</td>
                                <td>{{$info->status}}</td>
                                <td>
                                  <a href="/assetbid/<?php echo $info->id; ?>" class="btn btn-success">View</a>
                                  <a href="/assetbid/viewDetails/<?php echo $info->id; ?>" class="btn btn-success">View Details</a>
                                  <form action="{{ route('assetbid.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                  </form>&nbsp
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
                  @else
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>Id</th>
                            <th>Asset On Bid</th>
                            <th>Bid Amount</th>
                            <th>Seller</th>
                            <th>Status</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <td>{{$info->id}}</td>
                                <?php
                                $assetbids = \App\AssetBid::findOrFail($info->assetsBidId);
                                ?>
                                @if($assetbids->dealId)
                                <?php $deal = \App\Deal::findOrFail($assetbids->dealId); ?>
                                <td>{{$deal->dealName}}</td>
                                @elseif($assetbids->productId)
                                <?php $product = \App\Product::findOrFail($assetbids->productId); ?>
                                <td>{{$product->productName}}</td>
                                @endif
                                <td>{{$info->bidAmount}}</td>
                                <td>{{$assetbids->sellerId}}</td>
                                <td>{{$info->status}}</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
                  @endif
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
