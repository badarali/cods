@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Bids</h4>
                      <?php $message = "All Bids" ?>
                      <p class="category">Records regarding Bids table</p>
                      <?php
                      if(Auth::User()->role=="Super" || Auth::User()->role=="Admin"){
                        $data = \App\Bid::where('assetsBidId', $assetbid_id)->get();
                      }
                      else if(Auth::User()->role=="client"){

                      }
                      else{

                      }
                      ?>
                  </div>
                  <div class="content table-responsive table-full-width">
                    @if(Auth::User()->role == 'Super')
                    <?php
                    $assetbid = \App\AssetBid::findOrFail($assetbid_id);
                    $user = \App\User::findOrFail($assetbid->sellerId);
                    ?>
                    <div class="col-md-12">
                      <h5><b>Seller: </b><a href="/users/<?php echo $user->id;?>">{{$user->name}}</a></h5>
                    </div>
                    @endif
                      <table class="table table-striped">
                          <thead>
                            <th>Id</th>
                            <th>By User</th>
                            <th>Bid Amount</th>
                            <th>Status</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <tr>
                                <td>{{$info->id}}</td>
                                <?php $uploader = \App\User::findOrFail($info->byUserId); ?>
                                @if(Auth::User()->role == 'Super')
                                <td><a href="/users/<?php echo $uploader->id;?>">{{$uploader->name}}</a></td>
                                @else
                                <td>{{$uploader->name}}</td>
                                @endif
                                <td>{{$info->bidAmount}}</td>
                                <td>{{$info->status}}</td>

                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
