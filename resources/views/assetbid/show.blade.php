@extends('layouts.app')
@section('title','Single')
@section('content')
  <!-- banner-bootom-w3-agileits -->
<div class="banner-bootom-w3-agileits" id="app2">
	<div class="container">
		<div class="col-md-12 text-center showProductCategories">
			<?php
			$assetcategories = \App\AssetCategory::where('productId', $data->id)->get();
			$categoryids = array();
			foreach ($assetcategories as $key => $value) {
				if (!in_array($value->categoryId, $categoryids)) {
					$categories[] = \App\Category::where('id', $value->categoryId)->first();
					$categoryids[] = $value->categoryId;
				}
			}
			?>

			@if(!empty($categories))
			<div>
			<?php foreach ($categories as $key => $value): ?>
				<small><a href="/product/<?php echo "$value->categoryName/"; echo "$value->id"; ?>" style="display:inline; background-color:#348e37; padding:1%; margin-right:1%; color:white">{{$value->categoryName}}</a></small><?php if (sizeof($categories)-1>$key): ?>

				<?php endif; ?>
			<?php endforeach; ?>
		</div>
			@endif
		</div>
	   <div class="col-md-4 single-right-left ">
			<div class="grid images_3_of_2">
				<div class="flexslider">
					<ul class="slides">
            <?php
						$productImages=\App\Media::where('productId',$data->id)->where('mediaType', 'Image')->get();
						$productVideos=\App\Media::where('productId',$data->id)->where('mediaType', 'Video')->get();
						?>
            <?php foreach ($productImages as $key => $value): ?>
              <li data-thumb="{{URL::asset('product/images/').'/'.$value->mediaPath}}">
  							<div class="thumb-image"> <img src="{{URL::asset('product/images/').'/'.$value->mediaPath}}" data-imagezoom="true" class="img-responsive"> </div>
  						</li>
            <?php endforeach; ?>
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-8 single-right-left simpleCart_shelfItem">
			<?php
			$data->productVisits = $data->productVisits + 1;
			$data->save();
			?>
					<h3>{{$data->productName}}
						<?php
						$checkFavourite = \App\Favourite::where('byUserId', Auth::id())->where('toProductId', $data->id)->first();
						?>
						@if(!empty($checkFavourite))
						<form action="{{ route('favourite.destroy', $checkFavourite->id) }}" method="post" style="display:inline">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<input type="text" value="{{$data->id}}" name="productId" hidden>
							<input type="text" value="Product" name="type" hidden>
							<button style="border:none; background-color:white" class="favLinkChecked" type="submit"><i class="fas fa-heart" title="Remove From Favourite"></i></button>
						</form>
						@else
						<form action="/favourite" method="post" style="display:inline">
              {{ csrf_field() }}
              <input type="text" value="{{Auth::id()}}" name="byUserId" hidden>
              <input type="text" value="{{$data->id}}" name="toProductId" hidden>
              <input type="text" value="Product" name="type" hidden>
              <button style="border:none ; background-color:white" class="favLink" type="sumbit"><i class="fas fa-heart" title="Favourite"></i></button>
            </form>
						@endif

						<?php
						$checkFlag = \App\Flag::where('byUserId', Auth::id())->where('toProductId', $data->id)->first();
						?>
						@if(!empty($checkFlag))
						<form action="{{ route('flag.destroy', $checkFlag->id) }}" method="post" style="display:inline">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<input type="text" value="{{$data->id}}" name="productId" hidden>
							<input type="text" value="Product" name="type" hidden>
							<button style="border:none; background-color:white" class="flagLinkChecked" type="submit"><i class="fas fa-flag" title="Remove From Flag"></i></button>
						</form>
						@else
						<form action="/flag" method="post" style="display:inline">
              {{ csrf_field() }}
              <input type="text" value="{{Auth::id()}}" name="byUserId" hidden>
              <input type="text" value="{{$data->id}}" name="toProductId" hidden>
            	<input type="text" value="Product" name="type" hidden>
              <button style="border:none ; background-color:white" class="flagLink" type="submit"><i class="fas fa-flag" title="Flag"></i></button>
            </form>
						@endif
						<div v-text="remainingTime" v-if="!isExpire">
						</div>
						<div><h4><b>Base Amount: </b>{{$assetbid->baseAmount}}</h4></div>
            <div class=" col-md-6 pull-right">
							<h3 class="text-center">Bidding</h3><br>
              <form action="/bid" method="POST" enctype="multipart/form-data" v-if="!isExpire">
                {{ csrf_field() }}
                <input value="{{$assetbid->id}}" name="assetBidId" hidden>
                <input type="number" placeholder="Bid Amount" name="bidAmount" min="{{$assetbid->baseAmount}}"><br><br>
                <input type="submit" value="Submit" class="btn btn-success pull-right" />
              </form>
              <div style="height:250px; overflow-y:auto; width:100%; background-color:lightgray; padding:5%">
								<div v-for="(bid, index) in bids"><p v-text="users[index].name" style="margin:0px;"></p><small v-text="bid.bidAmount"></small><hr></div>
            </div>
						</div>
					</h3>
												<?php
												$rates = \App\Reviewandrating::where('toProductId', $data->id)->get();
												if (!$rates->isEmpty()) {
													$count = \App\Reviewandrating::where('toProductId', $data->id)->count();
													$stars = 0;
													foreach ($rates as $key => $value) {
														$stars = $stars + $value->stars;
													}
													$avg = $stars / $count;
													$fullStars = $avg*10;
													$halfStar = 0;
													if ($fullStars%10 > 0) {
													 $halfStar = 1;
													}
													$fullStars = floor($avg);
													$starCount = $fullStars + $halfStar;
													$remain = 5 - $starCount;
													for ($i=0; $i < $fullStars; $i++) {
														echo '<i class="fas fa-star starChecked"></i>';
													}
													for ($i=0; $i < $halfStar; $i++) {
														echo '<i class="fas fa-star-half-alt starChecked"></i>';
													}
													for ($i=0; $i < $remain; $i++) {
														echo '<i class="fas fa-star starUnchecked"></i>';
													}
													echo " <small>($count Reviews)</small>";
												}
												?>
					<div class="description">
						<div class="color-quality-right">
							<h5>Description:</h5>
							{{$data->productDescriptionPath}}
							<?php foreach ($productVideos as $key => $value): ?>
								<br><br>
								<video class="col-md-12" controls>
								  <source src="{{URL::asset('product/videos/').'/'.$value->mediaPath}}" type="video/mp4">
								  Your browser does not support HTML5 video.
								</video>
							<?php endforeach; ?>
						</div>
					</div>
		     </div>
	 			<div class="clearfix"> </div>
				<!-- /new_arrivals -->
				<div class="col-md-12">
					<iframe :src="iframe" frameborder="0" style="border:0;" height="400em" class="col-md-12" allowfullscreen></iframe>
				</div>
	<div class="responsive_tabs_agileits">
				<div class="col-md-6">
						<ul class="resp-tabs-list">
							<li>Reviews</li>
						</ul>
					<div class="resp-tabs-container">
						<div class="tab2">

							<div class="single_page_agile_its_w3ls">
								<div class="bootstrap-tab-text-grids">
									@if($message=='Rate')
									<div class="bootstrap-tab-text-grid">
										<div class="add-review">
	 										<h4>add a review</h4>
	 										<form action="/reviewandrating" method="POST" enctype="multipart/form-data">
												{{ csrf_field() }}
	 											<input type="text" value="{{$data->id}}" hidden name="toProductId">
	 											<div class="rating1">
	 												<fieldset class="starability-basic" style="min-height:auto;">
	 												  <input type="radio" id="no-rate" class="input-no-rate" name="rating" value="0" checked aria-label="No rating." />
	 												  <input type="radio" id="first-rate1" name="rating" value="1" />
	 												  <label for="first-rate1" title="Terrible">1 star</label>
	 												  <input type="radio" id="first-rate2" name="rating" value="2" />
	 												  <label for="first-rate2" title="Not good">2 stars</label>
	 												  <input type="radio" id="first-rate3" name="rating" value="3" />
	 												  <label for="first-rate3" title="Average">3 stars</label>
	 												  <input type="radio" id="first-rate4" name="rating" value="4" />
	 												  <label for="first-rate4" title="Very good">4 stars</label>
	 												  <input type="radio" id="first-rate5" name="rating" value="5" />
	 												  <label for="first-rate5" title="Amazing">5 stars</label>
	 												</fieldset>
	 											</div>
												<input type="text" value="Product" name="type" hidden>
	 											<textarea name="review" placeholder="Review"></textarea>
	 											<input type="submit" value="SEND">
	 										</form>
	 									</div>
									</div>
									@endif
									<?php
									$reviews = \App\Reviewandrating::where('toProductId', $data->id)->get();
									?>
									<?php foreach ($reviews as $key => $value): ?>
									<?php $userImage=\App\Media::where('userProfileId',$value->byUserId)->first(); ?>

									<div class="bootstrap-tab-text-grid">
										<?php if ($userImage): ?>
											<div class="bootstrap-tab-text-grid-left">
												<img src="{{URL::asset('profile/images/').'/'.$userImage->mediaPath}}" alt=" " class="img-responsive">
											</div>
										<?php else: ?>
											<div class="bootstrap-tab-text-grid-left">
												<i class="fas fa-user"></i>
											</div>
										<?php endif; ?>
											<div class="bootstrap-tab-text-grid-right">
												<ul>
													<?php
													$user = \App\User::where('id', $value->byUserId)->first();
													?>
													<li>
														@if(Auth::User()->role=='Super')
														<a href="/users/<?php echo $user->id;?>">{{$user->name}}</a>
														@else
														{{$user->name}}
														@endif
													</li>
													<li>
														<?php
														for ($i=0; $i < $value->stars; $i++) {
															echo '<i class="fas fa-star starChecked"></i>';
														}
														$remain = 5 - $value->stars;
														for ($i=0; $i < $remain; $i++) {
															echo '<i class="fas fa-star starUnchecked"></i>';
														}
														?>
													</li>
												</ul>
												<p>{{$value->reviewPath}}</p>
											</div>

										<div class="clearfix"> </div>
						       </div>
									 <?php endforeach; ?>
								 </div>
							 </div>
						 </div>
					</div>
				</div>

				<div class="card col-md-6">
					<h3 style="margin-bottom:2%;">Comments</h3>
					<form action="/livecomment" method="POST" enctype="multipart/form-data" v-if="!isExpire">
						{{ csrf_field() }}
						<div class="form-group">
							<input value="{{Auth::id()}}" name="byUserId" hidden>
							<input value="{{$assetbid->id}}" name="assetbid_id" hidden>
							<input type="text" placeholder="Enter Your Comment" class="col-md-12" name="comment">
							<input type="submit" value="Comment" class="btn btn-success" style="margin-top:2%;">
						</div>
					</form>
					<div style="border:1px solid lightgray; padding:2%;">
						<div v-for="(comment, index) in comments">
							<b v-text="userCommented[index].name"></b><p v-text="comment.comment"></p><hr>
						</div>
					</div>
				</div>
			</div>
	<!-- //new_arrivals -->
	  	<!--/slider_owl-->

			<div class="w3_agile_latest_arrivals col-md-12">
				<h3 class="wthree_text_info">Similar <span>Products</span></h3>
				<?php foreach ($products as $key => $value): ?>
					<div class="col-md-3 product-men">
						<div class="men-pro-item simpleCart_shelfItem">
							<div class="men-thumb-item">
								<?php $profileImage=\App\Media::where('productId',$value->id)->first(); ?>
								@if(!empty($profileImage))
								<img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
								<img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
								@else
								<i class="ti-view-list-alt"></i>
								@endif
									<div class="men-cart-pro">
										<div class="inner-men-cart-pro">
											<a href="single.html" class="link-product-add-cart">Quick View</a>
										</div>
									</div>
									<span class="product-new-top">New</span>
							</div>
							<div class="item-info-product ">
								<h4><a href="/products/<?php echo $value->id; ?>">{{$value->productName}}</a></h4>
								<?php
								$rates = \App\Reviewandrating::where('toProductId', $value->id)->get();
								if (!$rates->isEmpty()) {
									$count = \App\Reviewandrating::where('toProductId', $value->id)->count();
									$stars = 0;
									foreach ($rates as $key => $value1) {
										$stars = $stars + $value1->stars;
									}
									$avg = $stars / $count;
									$fullStars = $avg*10;
									$halfStar = 0;
									if ($fullStars%10 > 0) {
									 $halfStar = 1;
									}
									$fullStars = floor($avg);
									$starCount = $fullStars + $halfStar;
									$remain = 5 - $starCount;
									for ($i=0; $i < $fullStars; $i++) {
										echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
									}
									for ($i=0; $i < $halfStar; $i++) {
										echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;></i>';
									}
									for ($i=0; $i < $remain; $i++) {
										echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
									}
									echo "<br><small>($count Reviews)</small>";
								}
								?>
								<div class="info-product-price">
									<span class="item_price">Rs {{$value->productTotalPrice}}</span>
									<del>Rs {{$value->productUnitPrice}}</del>
									<?php
									$discount = $value->productUnitPrice-$value->productTotalPrice;
									?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<div class="clearfix"> </div>
					<!--//slider_owl-->
		  </div>
	  </div>
 </div>

@endsection
