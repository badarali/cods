@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Category</h4>
                    </div>
                    <div class="content">
                        <form action="/category" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Category Name</label>
                                        <input type="text" class="form-control border-input" name="categoryName" placeholder="Enter Category Name" required/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Category Status</label>
                                        <select class="form-control border-input" name="categorystatus" required>
                                          <option value="Active">Active</option>
                                          <option value="Deactive">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Primary Category</label>
                                        <select class="form-control border-input" name="isPrimary" required>
                                          <option value="1">Yes</option>
                                          <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Category Description</label>
                                      <textarea class="form-control border-input" name="categoryDescription" cols="8" rows="8" placeholder="Enter Category Description"></textarea>
                                  </div>
                                </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">Add Category</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
