@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">{{$data->id}}. {{$data->categoryName}} <small>({{$data->categorystatus}})</small></h4>
                    </div>
                    <div class="content">
                        <p class="category">{{$data->categoryDescription}}</p>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
