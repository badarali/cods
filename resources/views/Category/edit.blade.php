@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Category</h4>
                    </div>
                    <div class="content">
                        <form action="/category/<?php echo $data->id;?>" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          {{method_field('PUT')}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Category Name</label>
                                        <input type="text" class="form-control border-input" placeholder="{{$data->name}}" value="{{$data->categoryName}}" name="categoryName">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label>Category Status</label>
                                      <select type="text" class="form-control border-input" name="categorystatus" required>
                                        @if($data->categorystatus=='Active')
                                        <option value="Active" selected>Active</option>
                                        <option value="Deactive">Deactive</option>
                                        @else
                                        <option value="Active">Active</option>
                                        <option value="Deactive" selected>Deactive</option>
                                        @endif
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label>Primary Category</label>
                                      <select class="form-control border-input" name="isPrimary" required>
                                        @if($data->isPrimary==1)
                                        <option value="1" selected>Yes</option>
                                        <option value="0">No</option>
                                        @else
                                        <option value="1">Yes</option>
                                        <option value="0" selected>No</option>
                                        @endif
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                    <label>Category Description</label>
                                    <textarea class="form-control border-input" name="categoryDescription" cols="8" rows="8">{{$data->categoryDescription}}</textarea>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-fill btn-wd">Update Category</button>
                                </div>
                              </div>
                            </div>
                        </form>
                      </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
