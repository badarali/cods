@extends('layouts.app')
@section('title','Home Page')
@section('content')

<div class="container-fluid" style="background-color:#E8E2E3">
  <div class="row">
    <div class="col-md-12 text-center">
      <br><h2>Contact Us</h2><br>
    </div>
    <?php
    $front = \App\Front::first();
    ?>
    <div class="col-md-offset-1 col-md-5">
      <form action="/sendMail" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="col-md-12">
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control border-input" name="name" placeholder="Name" required/>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control border-input" name="email" placeholder="Email" required/>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Subject</label>
            <input type="text" class="form-control border-input" name="subject" placeholder="Subject" required/>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Message</label>
            <textarea type="text" rows="8" class="form-control border-input" name="message" placeholder="Message" required/></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <button type="submit" class="btn btn-block btn-success">Submit</button>
        </div>
      </form>
    </div><br>
    <div class="col-md-5" style="background-color:white; padding:9%;box-shadow: 10px 10px 10px #aaa;">

      <table class="table text-center" style="font-size:20px;">
        <tr>
          <td><i class="fas fa-map-marker-alt"></i></td>
          <td>
            @if(!empty($front))
            {{$front->address}}
            @endif
          </td>
        </tr>
        <tr>
          <td>
            <i class="fas fa-phone"></i>
          </td>
          <td>
            @if(!empty($front))
            {{$front->contact}}
            @endif
          </td>
        </tr>
        <tr>
          <td>
            <i class="far fa-envelope"></i>
          </td>
          <td>
            @if(!empty($front))
            {{$front->email}}
            @endif
          </td>
        </tr>
      </table>
      <hr>
      @if(!empty($front))
      <ul class="social-nav model-3d-0 footer-social w3_agile_social">
        @if(!empty($front->youtube))
                        <li>
                          <a href="{{$front->youtube}}" class="pinterest">
                            <div class="back"><i class="fab fa-youtube-square"></i></div>
                            <div class="front"><i class="fab fa-youtube"></i></div>
                          </a>
                        </li>
                        @endif
                        @if(!empty($front->googlePlus))
                        <li>
                          <a href="{{$front->googlePlus}}" class="instagram">
                            <div class="back"><i class="fab fa-google-plus-g"></i></div>
                            <div class="front"><i class="fab fa-google-plus-g"></i></div>
                          </a>
                        </li>
                        @endif
                        @if(!empty($front->facebook))
                        <li>
                          <a href="{{$front->facebook}}" class="facebook">
                            <div class="front"><i class="fab fa-facebook-f" aria-hidden="true"></i></div>
                            <div class="back"><i class="fab fa-facebook-f" aria-hidden="true"></i></div>
                          </a>
                        </li>
                        @endif
                        @if(!empty($front->twitter))
                        <li>
                          <a href="{{$front->twitter}}" class="twitter">
                            <div class="front"><i class="fab fa-twitter" aria-hidden="true"></i></div>
                            <div class="back"><i class="fab fa-twitter" aria-hidden="true"></i></div>
                          </a>
                        </li>
                        @endif
                        @if(!empty($front->instagram))
                        <li>
                          <a href="{{$front->instagram}}" class="instagram">
                            <div class="front"><i class="fab fa-instagram" aria-hidden="true"></i></div>
                            <div class="back"><i class="fab fa-instagram" aria-hidden="true"></i></div>
                          </a>
                        </li>
                        @endif
                        @if(!empty($front->linkedin))
                        <li>
                          <a href="{{$front->linkedin}}" class="pinterest">
                            <div class="front"><i class="fab fa-linkedin-in" aria-hidden="true"></i></div>
                            <div class="back"><i class="fab fa-linkedin-in" aria-hidden="true"></i></div>
                          </a>
                        </li>
                        @endif
                      </ul>
                      @endif
    </div>
  </div><br>
</div>
@endsection
