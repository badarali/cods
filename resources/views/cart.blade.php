@extends('layouts.app')
@section('title','Home Page')
@section('content')

<div class="container-fluid" id="app1">
  <div class="row">
    <div class="col-md-offset-2 col-md-8 product-men">
      <div class="col-md-12 text-center">
        <h3>Cart Details</h3><br>
      </div>
      <table class="table table-striped table-hover table-bordered">
        <thead>
          <tr>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Control Section</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(i,index) in id">
            <td><a :href="links[index]" v-text="names[index]"></a></td>
            <td><input type="number" v-model="count[index]" class="form-control" @change="countChanged($event, count[index], index)" style="width:50%"></td>
            <td v-text="price[index]"></td>
            <td><button class="btn btn-danger btn-sm" @click="removeClicked($event, index)">Remove</button></td>
          </tr>
        </tbody>
      </table>
      @if(Auth::User())
      <div class="col-md-12" v-if="id.length>0">
        <div class="col-md-12 text-right">
          <a class="btn btn-success" href="/users/<?php echo Auth::id() ;?>">Add New Address/Contact</a>
        </div>
        <form action="/order" method="post">
          {{ csrf_field() }}
          <input type="text" v-model="stringifyid" name="id" hidden>
          <input type="text" v-model="stringifycount" name="count" hidden>
          <input type="text" v-model="stringifytype" name="type" hidden>

          <?php
          $user = Auth::User();
          $addresses = \App\Address::where('addressesOfUser', $user->id)->get();
          $contacts = \App\Contact::where('contactsOfUser', $user->id)->get();
          ?>
          <div class="col-md-6">
            <div class="form-group">
              <label>Address</label>
              <select class="form-control" name="address">
                <?php foreach ($addresses as $key => $value): ?>
                  <option value="{{$value->id}}">{{$value->addressesBody}}</option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Contacts</label>
              <select class="form-control" name="contact">
                <?php foreach ($contacts as $key => $value): ?>
                  <option value="{{$value->id}}">{{$value->contactsBody}}</option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="text-right col-md-12">
            <b>Total:</b> <p v-text="total" style="display:inline"></p>
          </div>
          <button class="btn btn-success" type="submit">Checkout</button>
        </form>
        </div>
        @else
        <div class="text-right col-md-12">
          <b>Total:</b> <p v-text="total" style="display:inline"></p>
        </div>
        <a class="btn btn-success" href="/login">Checkout</a>
      </div>
      @endif
    </div>
  </div>
</div>
@endsection
