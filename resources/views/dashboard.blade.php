@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
@if(Auth::User())
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="padding:20%;">
                        <h1 class="title">Welcome To Gagfah Store</h1><br>
                        <div class="text-center">
                          <a href="/home" class="tect-center btn btn-success">Back To Store</a>
                        </div>
                    </div>
                    <div class="content">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
