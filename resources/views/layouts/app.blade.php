<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Gagfah Store - @yield('title')</title>
<!--/tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--favicon-->
<link rel="shortcut icon" type="image/png" href="{{ URL::asset('assets/img/logo.png')}}"/>

<!--end favicon-->
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="e-commerce, ecommerce, shop, shoping, online, products, product, deal, deals, order, online store, online shopping, online shop" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--//tags -->
<!-- Bootstrap core CSS     -->
<link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{ URL::asset('assets/css/front/flexslider.css')}}" type="text/css" media="screen" />

<link href="{{ URL::asset('assets/css/front/style.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ URL::asset('assets/css/front/font-awesome.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('assets/css/front/starability-all.min.css')}}" type="text/css">
<link href="{{ URL::asset('assets/css/front/easy-responsive-tabs.css')}}" rel='stylesheet' type='text/css'/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<!-- //for bootstrap working -->
<!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'> -->
</head>
<body style="font-family: 'Roboto', sans-serif;">
<div class="header" id="home">
	<div class="container">
		<ul>
			@if (Auth::guest())
		  <li> <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Sign In </a></li>
			<li> <a href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sign Up </a></li>
			@else
		      <li class="dropdown">
		          <a href="/dashboard">
		              {{ Auth::user()->name }}
		          </a>
		      </li>
			@endif
			<?php
			$front = \App\Front::first();
			?>
			<li><i class="fas fa-phone-volume" aria-hidden="true"></i> Call : @if($front){{$front->contact}}@endif</li>
			<li><i class="far fa-envelope" aria-hidden="true"></i> <a href="#">Mail: @if($front){{$front->email}}@endif</a></li>
			<li><i class="fas fa-map-marker-alt"></i> <a href="#">Address: @if($front){{$front->address}}@endif</a></li>
		</ul>
	</div>
</div>
<!-- //header -->
<!-- header-bot -->
<div class="header-bot">
	<div class="header-bot_inner_wthreeinfo_header_mid">

		<!-- header-bot -->
			<div class="col-md-6 logo_agile">
				@if($front)
				<img src="{{asset('logo/images/').'/'.$front->logoPath}}" width="70%" height="auto"/>
				@endif
			</div>

        <!-- header-bot -->
				@if($front)
		<div class="col-md-6 agileits-social top_content">
						<ul class="social-nav model-3d-0 footer-social w3_agile_social">
						   <li class="share">Share On : </li>
														@if(!empty($front->youtube))
															<li>
																<a href="{{$front->youtube}}" class="pinterest">
								 									<div class="back"><i class="fab fa-youtube-square"></i></div>
								 									<div class="front"><i class="fab fa-youtube"></i></div>
								 								</a>
								 							</li>
															@endif
															@if(!empty($front->googlePlus))
															<li>
																<a href="{{$front->googlePlus}}" class="instagram">
								 									<div class="back"><i class="fab fa-google-plus-g"></i></div>
								 									<div class="front"><i class="fab fa-google-plus-g"></i></div>
								 								</a>
								 							</li>
															@endif
															@if(!empty($front->facebook))
															<li>
																<a href="{{$front->facebook}}" class="facebook">
																  <div class="front"><i class="fab fa-facebook-f" aria-hidden="true"></i></div>
																  <div class="back"><i class="fab fa-facebook-f" aria-hidden="true"></i></div>
																</a>
															</li>
															@endif
															@if(!empty($front->twitter))
															<li>
																<a href="{{$front->twitter}}" class="twitter">
																  <div class="front"><i class="fab fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fab fa-twitter" aria-hidden="true"></i></div>
																</a>
															</li>
															@endif
															@if(!empty($front->instagram))
															<li>
																<a href="{{$front->instagram}}" class="instagram">
																  <div class="front"><i class="fab fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fab fa-instagram" aria-hidden="true"></i></div>
																</a>
															</li>
															@endif
															@if(!empty($front->linkedin))
															<li>
																<a href="{{$front->linkedin}}" class="pinterest">
																  <div class="front"><i class="fab fa-linkedin-in" aria-hidden="true"></i></div>
																  <div class="back"><i class="fab fa-linkedin-in" aria-hidden="true"></i></div>
																</a>
															</li>
															@endif
														</ul>
		</div>
		@endif
		<div class="clearfix"></div>
	</div>
</div>
<!-- //header-bot -->
<!-- banner -->
<div class="ban-top">
	<div class="container">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
					<li class="active menu__item menu__item--current"><a class="menu__link" href="/home">Home <span class="sr-only">(current)</span></a></li>
					<li class=" menu__item"><a class="menu__link" href="/home">Products</a></li>
					<li class=" menu__item"><a class="menu__link" href="/websiteDeals">Deals</a></li>

					<li class=" menu__item"><a class="menu__link" href="/websiteBids">Bids</a></li>
					<!-- <li class=" menu__item"><a class="menu__link" href="/bid">Bid</a></li> -->
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
							<ul class="dropdown-menu multi-column columns-3" style="background-color:#ce1f1d;">
								<div class="agile_inner_drop_nav_info">

									<div class="col-sm-6 multi-gd-img">
										<ul class="multi-column-dropdown">
											<?php
											$categories = \App\Category::where('isPrimary','1')->where('categorystatus', 'Active')->get();
											?>
											<?php foreach ($categories as $key => $value): ?>
												<li class="hoverable"><a class="categorylist" style="color:white" href="/product/<?php echo "$value->categoryName/"; echo "$value->id"; ?>">{{$value->categoryName}}</a>
													<?php $childs = \App\Subcategory::where('parentCategory',$value->id)->where('categorystatus', 'Active')->get(); ?>
													@if($childs)
													<ul class="afterhover">
													<?php foreach ($childs as $key => $value1): ?>
														<?php $child = \App\Category::findOrFail($value1->childCategory); ?>
														@if($child->categorystatus=='Active')
														<li style="margin-left:2em"><a class="categorylist" style="color:white" href="/product/<?php echo "$child->categoryName/"; echo "$child->id"; ?>">{{$child->categoryName}}</a></li>
														<?php $childschild = \App\Subcategory::where('parentCategory',$child->id)->where('categorystatus', 'Active')->get(); ?>
														@if($childschild)
														<ul style="margin-left:2em; border-bottom:1px solid;">
														<?php foreach ($childschild as $key => $value2): ?>
															<?php $childschildCategory = \App\Category::findOrFail($value2->childCategory); ?>
																<li style="margin-left:2em;"><a class="categorylist" style="color:white" href="/product/<?php echo "$childschildCategory->categoryName/"; echo "$childschildCategory->id"; ?>">{{$childschildCategory->categoryName}}</a></li>
														<?php endforeach; ?>
														</ul>
														@endif
														@endif
													<?php endforeach; ?>
													</ul>
													@endif
												</li>
											<?php endforeach; ?>
										</ul>
									</div>

									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
					<li class=" menu__item"><a class="menu__link" href="/contact">Contact</a></li>
					<li class=" menu__item">
						<a id="cart-link" href="/cart" class="w3view-cart" type="button" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true" id="cart-icon"></i></a>
					</li>
				  </ul>
				</div>
			  </div>
			</nav>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //banner-top -->
<!-- Modal1 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-12 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign In <span>Now</span></h3>

                <form class="form-horizontal" style="background-color:#f9f9f9; padding:2%;" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4">E-Mail Address</label>

                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4">Password</label>

                        <div class="col-md-8">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
<!--
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                </label>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-8">
                            <input type="submit" class="btn btn-primary"/>
                            <!-- <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                Forgot Your Password?
                            </a> -->
                        </div>
                    </div>
                </form>
								<div class="clearfix"></div>
								<p><a href="#" data-toggle="modal" data-target="#myModal2" > Don't have an account?</a></p>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-- //Modal1 -->
<!-- Modal2 -->
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-12 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign Up <span>Now</span></h3>

                <form class="form-horizontal" style="background-color:#f9f9f9; padding:2%;" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-6">
                            <input type="submit" class="btn btn-primary"/>

                        </div>
                    </div>
                </form>

														<div class="clearfix"></div>
														<p><a href="#">By clicking register, I agree to your terms</a></p>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-- //Modal2 -->

@yield('content')
<hr/>
<!--/grids-->
<div class="coupons">
		<div class="coupons-grids text-center">
			<div class="w3layouts_mail_grid">
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-truck" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3 style="padding-top:15%;">FREE SHIPPING</h3>

					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-headphones" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3 style="padding-top:15%;">24/7 SUPPORT</h3>

					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-shopping-bag" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3 style="padding-top:15%;">MONEY BACK GUARANTEE</h3>

					</div>
				</div>
					<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-gift" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3 style="padding-top:15%;">FREE GIFT COUPONS</h3>

					</div>
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>
</div>
<!--grids-->
<!-- footer -->
<div class="footer">
	<div class="footer_agile_inner_info_w3l">

		<div class="clearfix"></div>

		<p class="copy-right" style="color:white;">&copy Design by <a href="http://w3layouts.com/">W3layouts</a> | Coded by <a href="#">ONSETS</a></p>
	</div>
</div>
<!-- //footer -->

<!-- login -->
			<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body modal-spa">
							<div class="login-grids">
								<div class="login">
									<div class="login-bottom">
										<h3>Sign up for free</h3>
										<form>
											<div class="sign-up">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">
											</div>
											<div class="sign-up">
												<h4>Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">

											</div>
											<div class="sign-up">
												<h4>Re-type Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">

											</div>
											<div class="sign-up">
												<input type="submit" value="REGISTER NOW" >
											</div>

										</form>
									</div>
									<div class="login-right">
										<h3>Sign in with your account</h3>
										<form>
											<div class="sign-in">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">
											</div>
											<div class="sign-in">
												<h4>Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												<a href="#">Forgot password?</a>
											</div>
											<div class="single-bottom">
												<input type="checkbox"  id="brand" value="">
												<label for="brand"><span></span>Remember Me.</label>
											</div>
											<div class="sign-in">
												<input type="submit" value="SIGNIN" >
											</div>
										</form>
									</div>
									<div class="clearfix"></div>
								</div>
								<p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- //login -->
<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

<!-- js -->
<script type="text/javascript" src="{{ URL::asset('js/front/jquery-2.1.4.min.js')}}"></script>
<!-- //js -->
@if(!empty($assetbid))
<script>
	var assetBid = {!! json_encode($assetbid) !!};
</script>
@endif
<script type="text/javascript" src="{{ URL::asset('js/front/vue.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/bootstrap-notify.min.js')}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/vuefile.js') }}"></script>
<script src="{{ URL::asset('js/front/modernizr.custom.js')}}"></script>
	<!-- Custom-JavaScript-File-Links -->
	<!-- cart-js -->
	<script src="{{ URL::asset('js/front/minicart.min.js')}}"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>

	<!-- //cart-js -->
<!-- script for responsive tabs -->
<script src="{{ URL::asset('js/front/easy-responsive-tabs.js')}}"></script>
<script>
	$(document).ready(function () {
	$('#horizontalTab').easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion
	width: 'auto', //auto or any width like 600px
	fit: true,   // 100% fit in a container
	closed: 'accordion', // Start closed if in accordion view
	activate: function(event) { // Callback function if tab is switched
	var $tab = $(this);
	var $info = $('#tabInfo');
	var $name = $('span', $info);
	$name.text($tab.text());
	$info.show();
	}
	});
	$('#verticalTab').easyResponsiveTabs({
	type: 'vertical',
	width: 'auto',
	fit: true
	});

	});
</script>
<!-- //script for responsive tabs -->
<!-- stats -->
	<script src="{{ URL::asset('js/front/jquery.waypoints.min.js')}}"></script>
	<script src="{{ URL::asset('js/front/jquery.countup.js')}}"></script>
	<script>
		$('.counter').countUp();
	</script>
<!-- //stats -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{ URL::asset('js/front/move-top.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/jquery.easing.min.js')}}"></script>
<!-- FlexSlider -->
<script src="{{ URL::asset('js/front/jquery.flexslider.js')}}"></script>
						<script>
						// Can also be used with $(document).ready()
							$(window).load(function() {
								$('.flexslider').flexslider({
								animation: "slide",
								controlNav: "thumbnails"
								});

							});

						</script>
					<!-- //FlexSlider-->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

			});
	</script>
	<script>
 function startTime() {
   //alert("hello");
     var today = new Date();
     var h = today.getHours();
     var m = today.getMinutes();
     var s = today.getSeconds();
     m = checkTime(m);
     s = checkTime(s);
     document.getElementById('txt').innerHTML =
     h + ":" + m + ":" + s;
     var t = setTimeout(startTime, 500);
 }
 function checkTime(i) {
     if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
     return i;
 }
 $(document).ready(function(){
     //alert("hello");
		 var today = new Date();
     var h = today.getHours();
     var m = today.getMinutes();
     var s = today.getSeconds();
     m = checkTime(m);
     s = checkTime(s);
     document.getElementById('txt').innerHTML =
     h + ":" + m + ":" + s;
     var t = setTimeout(startTime, 500);
 });
 </script>
<!-- //here ends scrolling icon -->
<!-- single -->
<script src="{{ URL::asset('js/front/imagezoom.js')}}"></script>
<!-- single -->

<!-- for bootstrap working -->
<script type="text/javascript" src="{{ URL::asset('js/front/bootstrap.js')}}"></script>
</body>
</html>
