<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/logo.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Gagfah Store - @yield('title')</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ URL::asset('assets/css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{ URL::asset('assets/css/paper-dashboard.css')}}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ URL::asset('assets/css/demo.css')}}" rel="stylesheet" />

		<link rel="stylesheet" href="{{ URL::asset('assets/css/front/starability-all.min.css')}}" type="text/css">
    <!--  Fonts and icons     -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ URL::asset('assets/css/themify-icons.css')}}" rel="stylesheet">

</head>
<body onload="myfucntion()">

<div class="wrapper">
    <div class="sidebar" data-background-color="black" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="/dashboard" class="simple-text">
                    {{Auth::User()->name}}
                </a>
            </div>
            <ul class="nav">
                <li>
                    <a href="/dashboard">
                        <i class="fas fa-spa"></i>
                        <p>Welcome</p>
                    </a>
                </li>
								@if(Auth::User()->role == 'Super')
                <li>
                    <a href="/users">
                        <i class="ti-user"></i>
                        <p>Users</p>
                    </a>
                </li>
								<li>
                    <a href="/category">
                        <i class="ti-menu-alt"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li>
                    <a href="/subcategory">
                        <i class="ti-line-double"></i>
                        <p>Sub Categories</p>
                    </a>
                </li>
								@endif
								@if(Auth::User()->role == 'Super' || Auth::User()->role == 'Admin')
                <li>
                    <a href="/products">
                        <i class="ti-view-list-alt"></i>
                        <p>Products</p>
                    </a>
                </li>
								@endif
								<li>
                    <a href="/flaged/product">
                        <i class="fas fa-flag"></i>
                        <p>Flaged Products</p>
                    </a>
                </li>
								<li>
                    <a href="/favourite/product">
                        <i class="fas fa-heart"></i>
                        <p>Favourite Products</p>
                    </a>
                </li>
								<li class="hidden">
                    <a href="/productindeals">
                        <i class="ti-gift"></i>
                        <p>Product in Deals</p>
                    </a>
                </li>
								@if(Auth::User()->role == 'Super' || Auth::User()->role == 'Admin')
								<li>
                    <a href="/deals">
                        <i class="ti-gift"></i>
                        <p>Deals</p>
                    </a>
                </li>
								@endif
								<li>
                    <a href="/flaged/deal">
                        <i class="fas fa-flag"></i>
                        <p>Flaged Deals</p>
                    </a>
                </li>
								<li>
                    <a href="/favourite/deal">
                        <i class="fas fa-heart"></i>
                        <p>Favourite Deals</p>
                    </a>
                </li>
                <li>
                    <a href="/order">
                        <i class="ti-clipboard"></i>
                        <p>Orders</p>
                    </a>
                </li>
								@if(Auth::User()->role!='Admin')
								<li>
                    <a href="/reviewandrating">
                        <i class="fas fa-star"></i>
                        <p>Review and Rating</p>
                    </a>
                </li>
								@endif
								<?php
								$front = \App\Front::all();
								?>
								@if(Auth::User()->role=='Super' && $front->isEmpty())
								<li>
                    <a href="front/create">
                        <i class="ti-clipboard"></i>
                        <p>Front Data</p>
                    </a>
                </li>
								@elseif(Auth::User()->role=='Super')
								<?php
								$front = \App\Front::first();
								?>
								<li>
                    <a href="/front/<?php echo $front->id;?>/edit">
                        <i class="far fa-window-maximize"></i>
                        <p>Front Data</p>
                    </a>
                </li>
								@endif
								<li>
                    <a href="/assetbid">
                        <i class="far fa-window-maximize"></i>
                        <p>Bids</p>
                    </a>
                </li>
								<li>
                    <a href="/logout">
                        <i class="ti-export"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Gagfah Store -Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
            <li>

                            <a href="/users/{{Auth::User()->id}}">
                <i class="ti-settings"></i>
                <p>Account Settings</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

@yield('content')

        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="/contact">
                                Support Center
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, Dashboard by <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
                    | Coded by <a href="https://www.facebook.com/onsetSoftwarecompany">Onsets</a>
                </div>
            </div>
        </footer>
      </div>
  </div>
  </body>

      <!--   Core JS Files   -->
      <script src="{{ URL::asset('assets/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ URL::asset('assets/js/bootstrap-checkbox-radio.js')}}"></script>

    <!--  Charts Plugin -->
    <script src="{{ URL::asset('assets/js/chartist.min.js')}}"></script>

      <!--  Notifications Plugin    -->
      <script src="{{ URL::asset('assets/js/bootstrap-notify.js')}}"></script>

      <!--  Google Maps Plugin    -->
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

      <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="{{ URL::asset('assets/js/paper-dashboard.js')}}"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ URL::asset('assets/js/demo.js')}}"></script>

		<script>
			function myfucntion(){
				demo.initChartist();
			}
		</script>
    <script type="text/javascript">
        $(document).ready(function(){



            $.notify({
                icon: @yield('icon'),
                message: @yield('message')

              },{
                  type: @yield('barcolor'),
                  timer: 4000
              });

        });
    </script>
  </html>
