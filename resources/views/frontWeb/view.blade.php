@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Data</h4>
                    </div>
                    <div class="content">
                        <form action="/deals" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Contact No</label>
                                  <input type="text" class="form-control border-input" name="contact" placeholder="Contact No" required/>
                              </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control border-input" name="email" placeholder="Email" required/>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Facebook Link</label>
                                  <input type="text" class="form-control border-input" placeholder="Facebook Link" name="facebook">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Google Plus Link</label>
                                  <input type="text" name="googlePlus" class="form-control border-input" placeholder="Google Plus Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Twitter Link</label>
                                  <input type="text" name="twitter" class="form-control border-input" placeholder="Twitter Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Instagram Link</label>
                                  <input type="text" name="instagram" class="form-control border-input" placeholder="Instagram Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>LinkedIn Link</label>
                                  <input type="text" name="linkedin" class="form-control border-input" placeholder="LinkedIn Link">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Logo</label>
                                  <input type="file" name="logo" class="form-control border-input" accept=".jpg,.jpeg,.png," />
                              </div>
                          </div>
                          <div class="pull-right">
                              <button type="submit" class="btn btn-success btn-fill btn-wd">Add Data</button>
                          </div>
                          <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
