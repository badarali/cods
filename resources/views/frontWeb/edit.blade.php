@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Front Data</h4>
                    </div>
                    <div class="content">
                      <?php if (!empty($front->logoPath)): ?>
                        <img class="col-md-12" src="{{asset('logo/images/').'/'.$front->logoPath}}" alt="..."/>
                      <?php endif; ?>
                        <form action="/front/<?php echo $front->id;?>" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          {{method_field('PUT')}}
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Contact No</label>
                                  <input value="{{$front->contact}}" type="text" class="form-control border-input" name="contact" placeholder="Contact No" required/>
                              </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input value="{{$front->email}}" type="text" class="form-control border-input" name="email" placeholder="Email" required/>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Address</label>
                                <input value="{{$front->address}}" type="text" class="form-control border-input" name="address" placeholder="Address" required/>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Facebook Link</label>
                                  <input value="{{$front->facebook}}" type="text" class="form-control border-input" placeholder="Facebook Link" name="facebook">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Google Plus Link</label>
                                  <input value="{{$front->googlePlus}}" type="text" name="googlePlus" class="form-control border-input" placeholder="Google Plus Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Twitter Link</label>
                                  <input value="{{$front->twitter}}" type="text" name="twitter" class="form-control border-input" placeholder="Twitter Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Instagram Link</label>
                                  <input value="{{$front->instagram}}" type="text" name="instagram" class="form-control border-input" placeholder="Instagram Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>LinkedIn Link</label>
                                  <input value="{{$front->linkedin}}" type="text" name="linkedin" class="form-control border-input" placeholder="LinkedIn Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Youtube Link</label>
                                  <input value="{{$front->youtube}}" type="url" name="youtube" class="form-control border-input" placeholder="Youtube Link">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Logo</label>
                                  <input type="file" name="logo" class="form-control border-input" accept=".jpg,.jpeg,.png," />
                              </div>
                          </div>
                          <div class="pull-right">
                              <button type="submit" class="btn btn-success btn-fill btn-wd">Update Data</button>
                          </div>
                          <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
