@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Data</h4>
                    </div>
                    <div class="content">
                        <form action="/front" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Contact No</label>
                                  <input type="tel" class="form-control border-input" name="contact" placeholder="Contact No" required/>
                              </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control border-input" name="email" placeholder="Email" required/>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control border-input" name="address" placeholder="Address" required/>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Facebook Link</label>
                                  <input type="url" class="form-control border-input" placeholder="Facebook Link" name="facebook">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Google Plus Link</label>
                                  <input type="url" name="googlePlus" class="form-control border-input" placeholder="Google Plus Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Twitter Link</label>
                                  <input type="url" name="twitter" class="form-control border-input" placeholder="Twitter Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Instagram Link</label>
                                  <input type="url" name="instagram" class="form-control border-input" placeholder="Instagram Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>LinkedIn Link</label>
                                  <input type="url" name="linkedin" class="form-control border-input" placeholder="LinkedIn Link">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Youtube Link</label>
                                  <input type="url" name="youtube" class="form-control border-input" placeholder="Youtube Link">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Logo <small>(Only JPEG, JPG, PNG Accepted)</small></label>
                                  <input type="file" name="logo" class="form-control border-input" accept=".jpg,.jpeg,.png," required />
                              </div>
                          </div>
                          <div class="pull-right">
                              <button type="submit" class="btn btn-success btn-fill btn-wd">Add Data</button>
                          </div>
                          <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
