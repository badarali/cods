@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Subcategory</h4>
                    </div>
                    <div class="content">
                        <form action="/subcategory/<?php echo $data->id;?>" method="POST" enctype="multipart/form-data">
                          {{method_field('PUT')}}
                          {{ csrf_field() }}
                          <?php $categories=\App\Category::where('categorystatus', 'Active')->get(); ?>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label>Parent Category</label>
                                    <select class="form-control border-input" name="parentCategory">
                                      @<?php foreach ($categories as $key => $value): ?>
                                        @if($data->parentCategory==$value->id)
                                        <option value="{{$value->id}}" selected>{{$value->categoryName}}</option>
                                        @else
                                        <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                        @endif
                                      <?php endforeach; ?>
                                    </select>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label>Child Category</label>

                                    <select class="form-control border-input" name="childCategory">
                                      @<?php foreach ($categories as $key => $value): ?>
                                        @if($data->childCategory==$value->id)
                                        <option value="{{$value->id}}" selected>{{$value->categoryName}}</option>
                                        @else
                                        <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                        @endif
                                      <?php endforeach; ?>
                                    </select>
                                </div>

                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Sub Category Status</label>
                                      <select class="form-control border-input" name="categorystatus">
                                        @if($data->categorystatus=='Active')
                                        <option value="Active" selected>Active</option>
                                        <option value="Deactive">Deactive</option>
                                        @else
                                        <option value="Active">Active</option>
                                        <option value="Deactive" selected>Deactive</option>
                                        @endif
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">Update Sub Category</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                      </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
