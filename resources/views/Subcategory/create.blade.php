@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="content">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Sub Category</h4>
                    </div>
                    <div class="content">
                        <form action="/subcategory" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <?php $categories=\App\Category::where('categorystatus', 'Active')->get(); ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Parent Category</label>
                                        <select class="form-control border-input" name="parentCategory">
                                          <?php foreach ($categories as $key => $value): ?>
                                            <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Child Category</label>

                                        <select class="form-control border-input" name="childCategory">
                                          <?php foreach ($categories as $key => $value): ?>
                                            <option value="{{$value->id}}">{{$value->categoryName}}</option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Sub Category Status</label>
                                        <select class="form-control border-input" name="categorystatus">
                                          <option value="Active">Active</option>
                                          <option value="Deactive">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">Add Sub Category</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
