@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">All Sub Categories</h4>
                      <p class="category">Records regarding Sub Category table</p>
                      <div class="pull-right">
                          <a class="btn btn-primary" href="{{URL::asset('subcategory/create')}}">Add New</a>
                      </div>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                          <thead>
                            <th>ID</th>
                            <th>Parent Category</th>
                            <th>Child Category</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($data as $info)
                              <?php
                                $child=\App\Category::where('id',$info->childCategory)->first();
                                $parent=\App\Category::where('id',$info->parentCategory)->first();
                              ?>
                              <tr>
                                <td>{{$info->id}}</td>
                                <td>{{$parent->categoryName}}</td>
                                <td>{{$child->categoryName}}</td>
                                <td>{{$info->categorystatus}}</td>

                                <td><a class="btn btn-warning" href="/subcategory/<?php echo $info->id;?>/edit">Edit</a> <form action="{{ route('subcategory.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                  <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                                </form>&nbsp</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>

                  </div>
              </div>
          </div>

        </div>
    </div>
</div>
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
