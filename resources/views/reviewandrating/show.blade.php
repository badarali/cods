@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
@if(Auth::User()->role == 'Super')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
                <div class="card">
                    <div class="header">
                      <?php
                      if (!empty($data->toProductId)) {
                        $item = \App\Product::findOrFail($data->toProductId);
                        $type = 'Product';
                      }
                      else {
                        $type = 'Deal';
                        $item = \App\Deal::findOrFail($data->toDealId);
                      }

                      ?>
                        <h4 class="title">{{$data->id}}. @if($type=='Product'){{$item->productName}} @else {{$item->dealName}} @endif</h4>
                    </div>
                    <div class="content">
                      <?php
                      $stars = $data->stars;
                      for ($i=0; $i < $stars; $i++) {
                        echo '<i class="fas fa-star starChecked"></i>';
                      }
                      $remain = 5 - $data->stars;
                      for ($i=0; $i < $remain; $i++) {
                        echo '<i class="fas fa-star starUnchecked"></i>';
                      }
                      ?>
                      <p>{{$data->reviewPath}}</p>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
@endif
@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
