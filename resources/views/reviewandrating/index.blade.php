@extends('layouts.dashboard')
@section('title','Dashboard')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">Review and Ratings</h4>
                      <p class="category">Records regarding Review/Rating table</p>
                  </div>
                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped">
                        @if(Auth::User()->role=='Super')
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Item Name</th>
                            <th>Type</th>
                            <th>Reviewed By</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($data as $key => $value): ?>
                            <tr>
                              <td>{{$value->id}}</td>
                              <?php
                              if (!empty($value->toProductId)) {
                                $item = \App\Product::findOrFail($value->toProductId);
                                $type = 'Product';
                              }
                              else{
                                $item = \App\Deal::findOrFail($value->toDealId);
                                $type = 'Deal';
                              }
                              $user = \App\User::findOrFail($value->byUserId);
                              ?>
                              @if($type=='Product')
                              <td><a href="/products/<?php echo $item->id; ?>">{{$item->productName}}</a></td>
                              <td>{{$type}}</td>
                              @else
                              <td><a href="/deals/<?php echo $item->id; ?>">{{$item->dealName}}</a></td>
                              <td>{{$type}}</td>
                              @endif
                              <td><a href="/users/<?php echo $user->id;?>">{{$user->name}}</a></td>
                              <td>
                                <a href="/reviewandrating/<?php echo $value->id; ?>" class="btn btn-success">View</a>
                                <form action="{{ route('reviewandrating.destroy', $value->id) }}" method="post" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-danger" placeholder="Delete" value="Delete"/>
                              </form>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                        @endif
                        @if(Auth::User()->role=='Client')
                          <thead>
                            <th>Title</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            @foreach($deals as $info)
                              <tr>
                                <td>{{$info->dealName}}</td>
                                <td>
                                  <a href="/deals/<?php echo $info->id; ?>" class="btn btn-success">View</a>
                                  <a href="/reviewandrating/<?php echo $info->id; ?>/deal" class="btn btn-info">Review/Rate</a>
                                </td>
                              </tr>
                            @endforeach
                            @foreach($products as $product)
                              <tr>
                                <td>{{$product->productName}}</td>
                                <td>
                                  <a href="/products/<?php echo $product->id; ?>" class="btn btn-success">View</a>
                                  <a href="/reviewandrating/<?php echo $product->id; ?>/product" class="btn btn-info">Review/Rate</a>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                          @endif
                      </table>
                  </div>
              </div>
          </div>

        </div>
    </div>
</div>

@endsection
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'success'
@endsection
