@extends('layouts.app')
@section('title','Home Page')
@section('content')

<!-- /new_arrivals -->
  <div class="new_arrivals_agile_w3ls_info" id="app3">
    <div class="container">
        <h3 class="wthree_text_info">Let's <span>hunt "Bids"</span></h3>
        <?php foreach ($data as $key => $value): ?>
          <?php
          if($now > $value->endTime){
            $bids = \App\Bid::where('assetsBidId', $value->id)->get();
            if($bids->isEmpty()){
                $value->status = "loss";
                $value->save();
              }
              else{
                $highestBid = \App\Bid::where('assetsBidId', $value->id)->orderBy('bidAmount', 'desc')->first();
                $highestBid->status = "win";
                $highestBid->save();
                $value->status = "win";
                $value->save();
              }
          }
          ?>
          @if($value->dealId)
          <?php $Deal = \App\Deal::findOrFail($value->dealId); ?>
          @elseif($value->productId)
          <?php $product = \App\Product::findOrFail($value->productId); ?>
          <div class="col-md-3 product-men">
            <div class="men-pro-item simpleCart_shelfItem">
              <div class="men-thumb-item">
                <?php $profileImage=\App\Media::where('productId',$product->id)->first(); ?>
                @if(!empty($profileImage))
                <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-front" />
                <img src="{{asset('product/images/').'/'.$profileImage->mediaPath}}" width="75px" height="auto" class="pro-image-back" />
                @else
                <i class="ti-view-list-alt"></i>
                @endif
                  <div class="men-cart-pro">
                    <div class="inner-men-cart-pro">
                      <a href="/assetbid/<?php echo $value->id; ?>" class="link-product-add-cart">Quick View</a>
                    </div>
                  </div>
                  <span class="product-new-top">New</span>
              </div>
              <div class="item-info-product ">
                <h4><a href="/assetbid/<?php echo $value->id; ?>">{{$product->productName}}</a></h4>
                <?php
                $rates = \App\Reviewandrating::where('toProductId', $product->id)->get();
                if (!$rates->isEmpty()) {
                  $count = \App\Reviewandrating::where('toProductId', $product->id)->count();
                  $stars = 0;
                  foreach ($rates as $key => $value1) {
                    $stars = $stars + $value1->stars;
                  }
                  $avg = $stars / $count;
                  $fullStars = $avg*10;
                  $halfStar = 0;
                  if ($fullStars%10 > 0) {
                   $halfStar = 1;
                  }
                  $fullStars = floor($avg);
                  $starCount = $fullStars + $halfStar;
                  $remain = 5 - $starCount;
                  for ($i=0; $i < $fullStars; $i++) {
                    echo '<i class="fas fa-star starChecked" style="font-size:15px;"></i>';
                  }
                  for ($i=0; $i < $halfStar; $i++) {
                    echo '<i class="fas fa-star-half-alt starChecked" style="font-size:15px;"></i>';
                  }
                  for ($i=0; $i < $remain; $i++) {
                    echo '<i class="fas fa-star starUnchecked" style="font-size:15px;"></i>';
                  }
                  echo " <small>($count)</small>";
                }
                ?>
                <div class="info-product-price">
                  <span class="item_price">Rs {{$product->productTotalPrice}}</span>
                  <del>Rs {{$product->productUnitPrice}}</del>
                  <?php
                  $discount = $product->productUnitPrice-$product->productTotalPrice;
                  ?>
                </div>
                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                    <fieldset>
                      <?php
                      $endTime = new DateTime($value->endTime);
                      $remainTime = $now->diff($endTime);
                      $remain = $remainTime->format('%h:%i:%s');
                      ?>
                      <input type="button" value="{{$remain}}" class="button" />
                    </fieldset>
                </div>
              </div>
            </div>
          </div>
          @endif
        <?php endforeach; ?>
      </div>
    </div>
  <!-- //new_arrivals -->
@endsection
