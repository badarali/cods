var app = new Vue({
	el: '#app',
	data: {

		name: "",
		id: [],
		count: [],
		type: [],
		price: [],
		names: [],
	},
	methods:{
		add: function(e, id, type, price, name){
			var order = localStorage.getItem("id");
			if (order!=null) {
				this.id = JSON.parse(localStorage.getItem("id"));
				this.count = JSON.parse(localStorage.getItem("count"));
				this.type = JSON.parse(localStorage.getItem("type"));
				this.price = JSON.parse(localStorage.getItem("price"));
				this.names = JSON.parse(localStorage.getItem("names"));
			}
			if (this.id.includes(id)) {
				var index = -1;
				for (var i = 0; i < this.id.length; i++) {
					if (this.id[i] == id && this.type[i]==type) {
						index = i;
					}
				}
				if(index!=-1){
					this.count[index] = this.count[index] + 1;
					this.price[index] = this.price[index] + price;
				}
				else{
					this.id.push(id);
					this.count.push(1);
					this.type.push(type);
					this.price.push(price);
					this.names.push(name);
				}
			}
			else{
				this.id.push(id);
				this.count.push(1);
				this.type.push(type);
				this.price.push(price);
				this.names.push(name);
			}
			localStorage.setItem("id", JSON.stringify(this.id));
			localStorage.setItem("price", JSON.stringify(this.price));
			localStorage.setItem("count", JSON.stringify(this.count));
			localStorage.setItem("type", JSON.stringify(this.type));
			localStorage.setItem("names", JSON.stringify(this.names));
			$.notify({
				// options
				message: 'Added to Cart!'
			},{
				// settings
				type: 'success',
				delay: 5000,
				timer: 1000,
			});
		},
	},
	mounted(){
		if ('<?php if (!empty($message)) {echo $message;} ?>' == 'Ordered') {
			$.notify({
				// options
				message: 'Your Order has been placed!'
			},{
				// settings
				type: 'success',
				delay: 5000,
				timer: 1000,
			});
			localStorage.removeItem("id");
			localStorage.removeItem("price");
			localStorage.removeItem("type");
			localStorage.removeItem("names");
			localStorage.removeItem("count");
		}
	}
})

var app = new Vue({
	el: '#app1',
	data: {
		id: [],
		type: [],
		count: [],
		price: [],
		stringifyid: [],
		stringifycount: [],
		stringifytype: [],
		names: [],
		total: 0,
		links: [],
	},
	methods:{
		countChanged: function(e, count, index){
			if (count <= 0) {
				this.count[index] = 1;
				$.notify({
					// options
					message: 'Count cannont be 0 or less than 0'
				},{
					// settings
					type: 'danger',
					delay: 5000,
					timer: 1000,
				});
			}
			else
			{
				this.count = JSON.parse(localStorage.getItem("count"));
				this.price = JSON.parse(localStorage.getItem("price"));
				var unitPrice = this.price[index]/this.count[index];
				this.count[index] = count;
				this.price[index] = unitPrice * this.count[index];
				this.total = 0;
				for (var i = 0; i < this.price.length; i++) {
					this.total = this.total + this.price[i];
				}
				this.stringifycount = JSON.stringify(this.count);
				localStorage.setItem("count", JSON.stringify(this.count));
				localStorage.setItem("price", JSON.stringify(this.price));
			}
		},
		removeClicked: function(e, index){
			this.count.splice(index, 1);
			this.type.splice(index, 1);
			this.price.splice(index, 1);
			this.id.splice(index, 1);
			this.names.splice(index, 1);
			localStorage.setItem("count", JSON.stringify(this.count));
			localStorage.setItem("price", JSON.stringify(this.price));
			localStorage.setItem("id", JSON.stringify(this.id));
			localStorage.setItem("names", JSON.stringify(this.names));
			localStorage.setItem("type", JSON.stringify(this.type));
			this.total = 0;
			for (var i = 0; i < this.price.length; i++) {
				this.total = this.total + this.price[i];
			}
		}
	},
	mounted(){
		this.id = JSON.parse(localStorage.getItem("id"));
		this.type = JSON.parse(localStorage.getItem("type"));
		for (var i = 0; i < this.id.length; i++) {
			if (this.type[i]=="Product") {
				var link = "/products/" + this.id[i];
				this.links.push(link);
			}
			else if (this.type[i]=="Deal") {
				var link = "/deals/" + this.id[i];
				this.links.push(link);
			}
		}
		this.names = JSON.parse(localStorage.getItem("names"));
		this.stringifyid = JSON.stringify(this.id);
		this.price = JSON.parse(localStorage.getItem("price"));
		for (var i = 0; i < this.price.length; i++) {
			this.total = this.total + this.price[i];
		}
		this.count = JSON.parse(localStorage.getItem("count"));
		this.stringifycount = JSON.stringify(this.count);
		this.stringifytype = JSON.stringify(this.type);
	}
})

var app = new Vue({
	el: '#app2',
	data: {
		setit: "",
		assetBid: "",
		apiGetBids: "/getBids",
		bids: [],
		isExpire: false,
		users: [],
		remainingTime: "",
		comments: [],
		userCommented: [],
		iframe: "",
	},
	methods:{
		loadData: function(){
			var bids1 = [];
			var comp = this;
			var instance = axios.create({
      });
      instance.get(this.apiGetBids+"/"+this.assetBid.id,{})
        .then(function (response) {
					comp.iframe = response.data.iframe;
					comp.remainingTime = response.data.remaining;
					if(response.data.isExpire){
						comp.isExpire = true;
						clearInterval(comp.setit);
					}
					if (response.data.bids.length != comp.bids.length) {
						comp.bids = response.data.bids;
						comp.users = response.data.users;
					}
					if(response.data.comments.length != comp.comments.length){
						comp.comments = response.data.comments;
						comp.userCommented = response.data.userCommented;
					}
        })
        .catch(function (error) {
          console.log(error);
        });
		}
	},
	mounted(){
		this.assetBid = assetBid;
		this.loadData();
		this.setit = setInterval(function () {
			this.loadData();
		}.bind(this), 500);
	}
})
