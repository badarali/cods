<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/websiteDeals', 'HomeController@websitedeal');
Route::get('/deal/{category}/{id}', 'DealController@dealsInCategory');
Route::get('/product/{category}/{id}', 'ProductController@productsInCategory');
Auth::routes();
Route::get('/logout', function(){
    if (Auth::user()) {
     Auth::logout();
     return view('welcome');
  }
});
Route::get('/dashboard','HomeController@dashboard');
Route::get('/home', 'HomeController@index');
Route::get('/cart', 'HomeController@cart');
Route::get('/contact', 'FrontController@contact');
Route::post('/sendMail', 'FrontController@sendMail');
Route::post('/order/address-contact', 'OrdermetaController@addressescontacts');
Route::get('/favourite/{type}','FavouriteController@favourites');
Route::get('/reviewandrating/{id}/{type}','ReviewandratingController@reviewrate');
Route::get('/flaged/{type}','FlagController@flaged');
Route::get('/websiteBids','BidController@websiteBids');
Route::get('/user/{id}/statusNeedChange','UserController@statusNeedChange');
Route::get('/getBids/{assetbid_id}', 'BidController@getBids');
Route::get('/assetbid/viewDetails/{assetbid_id}', 'BidController@getBidsOfAsset');
Route::post('/addMapLink', 'UserController@addMapLink');
Route::post('/deleteGoogleMapsLink', 'UserController@deleteGoogleMapsLink');
Route::resource('/users', 'UserController');
Route::resource('/addresses','AddressController');
Route::resource('/category','CategoryController');
Route::resource('/contacts','ContactController');
Route::resource('/deals','DealController');
Route::resource('/favourite','FavouriteController');
Route::resource('/media','MediaController');
Route::resource('/message','MessageController');
Route::resource('/messagemedia','MessagemediaController');
Route::resource('/order','OrdermetaController');
Route::resource('/products','ProductController');
Route::resource('/reviewandrating','ReviewandratingController');
Route::resource('/subcategory','SubcategoryController');
Route::resource('/support','SupportController');
Route::resource('/supportmedia','SupportmediaController');
Route::resource('/productindeals','ProductindealController');
Route::resource('/flag','FlagController');
Route::resource('/front','FrontController');
Route::resource('/bid','BidController');
Route::resource('/assetbid','AssetBidController');
Route::resource('/livecomment','LivecommentController');
